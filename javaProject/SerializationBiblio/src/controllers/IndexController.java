package controllers;

import java.util.Iterator;
import java.util.List;

import models.IndexLibrary;
import world.BibliotecaTDO;
import world.DeweyClassification;
import world.Serializacion;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class IndexController
{
    public static String getBook(String index){
        Serializacion ser = Serializacion.getInstance( );
        return ser.getGsonBiblioteca( ).toJson(ser.getObjetoBiblioteca().get(Integer.parseInt(index)));
    }
    
    public static String getStatistics(){
        Serializacion ser = Serializacion.getInstance( );
        
        JsonObject response = new JsonObject( );
        response.addProperty( "booksQuantity", ""+ser.getObjetoBiblioteca( ).size( ) );
        response.addProperty( "indexUniandes", ""+IndexLibrary.countIndexLibrary() );
        response.addProperty( "currentBook", ""+ser.getCurrentPosition() );
        return response.toString();
    }
    
    public static String getBookByTitle(String title){
    	
        BasicDBObject responseObject = new BasicDBObject();
        Gson gson = Serializacion.getInstance().getGsonBiblioteca();
        List<DBObject> libraryIndexed = IndexLibrary.getIndexBookByTitle(title);
        for( Iterator<DBObject> iterator = libraryIndexed.iterator( ); iterator.hasNext( ); )
        {
        	BasicDBObject bookObject = new BasicDBObject();
            DBObject dbObject = iterator.next( );
            bookObject.put("libraryHashPosition", dbObject);
            BibliotecaTDO bookDTO =  Serializacion.getInstance().getAtPosition(Integer.parseInt((String)dbObject.get("position")));
            bookObject.put("libraryBook", JSON.parse(gson.toJson(bookDTO)));
            bookObject.put("deweyInfo", IndexLibrary.getDeweyCategories(bookDTO.getClasificacionDewey()));
            
            bookObject.put("collectionBooks", IndexLibrary.getBookByTitleAndAuthor(bookDTO));
            responseObject.put((String)dbObject.get("position"), bookObject);
        }
        
        return JSON.serialize(responseObject);
    }
    
    public static String cleanVariables(){
        Serializacion ser = Serializacion.getInstance( );
        
        ser.borrarvariables();
        System.gc();
        JsonObject response = new JsonObject( );
        //response.addProperty( "indexUniandes", ""+ser.getObjetoBiblioteca( ).size( ) );
        return response.toString();
    }
    
    public static String getNextBook(){
    	Serializacion ser = Serializacion.getInstance( );
    	return ser.darSiguiente();
    }
}
