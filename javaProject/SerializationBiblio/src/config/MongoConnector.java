package config;

import com.mongodb.DB;
import com.mongodb.Mongo;

public class MongoConnector
{
    
    public static final String INDEX_COLLECTION = "index_library_uniandes";
    public static final String BOOKS_COLLECTION = "books_2";
    
    private static MongoConnector instance;
    
    private Mongo mongo;

    private DB mongoDB;
    
    public static MongoConnector getInstance(){
        if (instance == null)
            instance = new MongoConnector( );
        return instance;
    }
    
    public DB getMongoDB( )
    {
        return mongoDB;
    }
    
    public Mongo getMongo( )
    {
        return mongo;
    }
    
    public MongoConnector( )
    {
        try
        {
            mongo = new Mongo( "localhost", 27017 );
            mongoDB = mongo.getDB( "library" );
        }
        catch( Exception e )
        {
            e.printStackTrace();
        }
    }
}
