package world;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;


public class DeweyClassification
{
	
	public static final String REDUCED_ERROR = "Dewey number was reduced.";
	public static final String EMPTY_ERROR = "Dewey number not found.";
    
    public static BasicDBObject categoriasDewey(String dewey)
    {
    	BasicDBObject bookObject = new BasicDBObject();
    	bookObject.put("categories", null);
    	bookObject.put("errors", null);
    	
    	List<String> errors = new ArrayList<String>();
        List<String> cats = new ArrayList<String>( );
        boolean found = false;
        while (!found)
	        try
	        {
	            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
	            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
	            Document doc = dBuilder.parse("http://dewey.info/class/"+dewey+"/about.en.rdf");
	            
	            doc.getDocumentElement().normalize();
	            
	            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
	            NodeList nListPs = doc.getElementsByTagName("skos:broader");
	            NodeList nListCats = doc.getElementsByTagName("skos:prefLabel");
	            System.out.println("-----------------------");
	            while(nListPs.item( 0 )!=null)
	            {
	                System.out.println(doc.getDocumentURI( ));
	                Node nNode = nListCats.item(0);
	                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
	                    String nodevalue = getNodeValue( nNode );
	                         System.out.println(nodevalue);
	                         cats.add( nodevalue );
	                 }
	                Node nodoB = nListPs.item( 0 ) ;
	                Element eElement = (Element) nodoB;
	                String docBroader = eElement.getAttribute( "rdf:resource" );
	                Pattern attributePattern = Pattern.compile( "(http://dewey.info/class/[0-9\\.]+)", Pattern.MULTILINE );
	                Matcher codigo = attributePattern.matcher( docBroader );
	                if(codigo.find( ))
	                {
	                    docBroader = codigo.group(1);
	                }
	                System.out.println("cambio a: "+docBroader );
	                doc = dBuilder.parse(docBroader+"/about.en.rdf");
	                nListPs = doc.getElementsByTagName("skos:broader");
	                nListCats = doc.getElementsByTagName("skos:prefLabel");
	                System.out.println("-----------------------");
	            }
	            found = true;
	            bookObject.put("categories", cats);
	        }
	        catch( Exception e )
	        {
	        	errors.add(REDUCED_ERROR);
	        	dewey = dewey.replace(dewey.substring(dewey.length()-1), "");
	        	if (dewey.length() <= 4)
	        		found = true;
	        }

        if (cats.size()==0)
        	errors.add(EMPTY_ERROR);
        if (errors.size()>0)
        	bookObject.put("errors", errors);
        return bookObject;
            
    }
    
    private static String getNodeValue( Node node )
    {
        NodeList childNodes = node.getChildNodes( );
        for( int x = 0; x < childNodes.getLength( ); x++ )
        {
            Node data = childNodes.item( x );
            if( data.getNodeType( ) == Node.TEXT_NODE )
                return data.getNodeValue( );
        }
        return "";
    }
    
    public static void main( String[] args )
    {
    	System.out.println(categoriasDewey("325.96"));
    }
}
