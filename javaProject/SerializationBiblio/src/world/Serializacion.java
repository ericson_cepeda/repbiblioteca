package world;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.text.CharacterIterator;
import java.text.StringCharacterIterator;
import java.util.ArrayList;
import java.util.List;

import models.IndexLibrary;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mongodb.DBObject;

public class Serializacion
{
	private int posicion;
    private static Serializacion INSTANCE;
    public static final String SPECIAL = "([`\\~!@#$%\\^\\&\\*\\(\\)_\\+\\[\\]\\;\',\\./{}|:\"<>?])";
    
    private ArrayList<BibliotecaTDO> objetoBiblioteca;

    private Gson gsonBiblioteca;

    public static Serializacion getInstance(){
        if (INSTANCE == null)
            INSTANCE = new Serializacion( );
        return INSTANCE;
    }
    
    public Serializacion( )
    {
    	posicion = 0;
        gsonBiblioteca = new Gson( );
        objetoBiblioteca = new ArrayList<BibliotecaTDO>( );
        IndexLibrary.cleanLibaryIndex();
        setGsonBiblioteca( );
    }

    public int getCurrentPosition(){
    	return posicion;
    }
    
    public Gson getGsonBiblioteca( )
    {
        return gsonBiblioteca;
    }

    public boolean setGsonBiblioteca( )
    {
        try
        {
            //String rutaArchivo = "C:/Users/Kelvin/Desktop/colecSB_03Oct2012.txt";
            String rutaArchivo = "/home/libraryData/colecSB_03Oct2012.txt";

            // Abrimos el archivo
            FileInputStream fstream = new FileInputStream( rutaArchivo );
            // Creamos el objeto de entrada
            DataInputStream entrada = new DataInputStream( fstream );
            // Creamos el Buffer de Lectura
            BufferedReader buffer = new BufferedReader( new InputStreamReader( entrada ) );
            String strLinea;
            // Leer el archivo linea por linea
            while( ( strLinea = buffer.readLine( ) ) != null )
            {
                // System.out.println( strLinea );
                if( strLinea.contains( "Dewey:" ) )
                {
                    String[] dato = strLinea.split( ":" );
                    BibliotecaTDO objeto = new BibliotecaTDO( );
                    objeto.setClasificacionDewey(cambioCaracter(dato[ 1 ]) );
                    crearNuevoLibro( buffer, objeto );
                }

            }
            // Cerramos el archivo
            entrada.close( );
        }
        catch( Exception e )
        { // Catch de excepciones
            System.err.println( "Ocurrio un error: " + e.getMessage( ) );
        }
        return true;
    }

    private void crearNuevoLibro( BufferedReader buffer, BibliotecaTDO objeto )
    {
        try
        {
            String strLinea;
            boolean fin = false;
            // Leer el archivo linea por linea
            while( ( strLinea = buffer.readLine( ) ) != null && !fin )
            {
                if( strLinea.contains( "Autor Personal:" ) )
                {
                    ArrayList<String> datoAutores = new ArrayList<String>( );
                    String[ ] dato = strLinea.split( "Autor Personal:" );
                    // Leer el archivo linea por linea
                    String [ ]autores =  dato[ 1 ].split( "," );
                    for( String stringRta : autores )
                    {
                        datoAutores.add( cambioCaracter(stringRta.trim( )) );                        
                    }
                    objeto.setAutorPersonal( datoAutores );
                }
                else if( strLinea.contains( "Titulo:" ) )
                {
                    String[] dato = strLinea.split( "Titulo:" );
                    String[] autor = dato[ 1 ].split( "/" );
                    
                    objeto.setTitulo( cambioCaracter(autor[ 0 ]) );
                }
                else if( strLinea.contains( "Datos Publicacion:" ) )
                {
                    String[] dato = strLinea.split( "Datos Publicacion:" );
                    objeto.setDatosPublicacion(cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Desc. Fisica:" ) )
                {
                    String[] dato = strLinea.split( "Desc. Fisica:" );
                    objeto.setDescFisica(cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Contenido Parcial:" ) )
                {
                    String[] dato = strLinea.split( "Contenido Parcial:" );
                    objeto.setContenidoParcial(cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Contenido:" ) )
                {
                    String[] dato = strLinea.split( "Contenido:" );
                    objeto.setContenido(cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Restriccion Acceso:" ) )
                {
                    String[] dato = strLinea.split( "Restriccion Acceso:" );
                    objeto.setRestriccionAcceso( cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Ref. donacion:" ) )
                {
                    ArrayList<String> datoRefDonacion = new ArrayList<String>( );
                    // Leer el archivo linea por linea
                    do
                    {
                        String[] dato = strLinea.split( "Ref. donacion:" );
                        datoRefDonacion.add( cambioCaracter(dato[ 1 ]) );
                    } while( ( strLinea = buffer.readLine( ) ) != null && strLinea.contains( "Ref. donacion:" ) );
                    objeto.setRefDonacion( datoRefDonacion );

                }
                else if( strLinea.contains( "Nota de Bibliografia:" ) )
                {
                    String[] dato = strLinea.split( "Nota de Bibliografia:" );
                    objeto.setNotaDeBibliografia( cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Curso audiencia:" ) )
                {
                    ArrayList<String> datoCursoAudiencia = new ArrayList<String>( );
                    // Leer el archivo linea por linea
                    do
                    {
                        String[] dato = strLinea.split( "Curso audiencia:" );
                        datoCursoAudiencia.add( cambioCaracter(dato[ 1 ]) );
                    } while( ( strLinea = buffer.readLine( ) ) != null && strLinea.contains( "Curso audiencia:" ) );

                    objeto.setCursoAudiencia( datoCursoAudiencia );
                }
                else if( strLinea.contains( "Nota de Reproduccion:" ) )
                {
                    String[] dato = strLinea.split( "Nota de Reproduccion:" );
                    objeto.setNotaDeReproduccion( cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Nota General:" ) )
                {
                    String[] dato = strLinea.split( "Nota General:" );
                    objeto.setNotaGeneral( cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "En Biblioteca(s):" ) )
                {
                    String[] dato = strLinea.split( ":" );
                    objeto.setEnBiblioteca( cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Bulletin heading:" ) )
                {
                    String[] dato = strLinea.split( "Bulletin heading:" );
                    objeto.setBulletinHeading( cambioCaracter(dato[ 1 ]) );
                }
                else if( strLinea.contains( "Materia Personal:" ) )
                {
                    ArrayList<String> datoMateriaPersonal = new ArrayList<String>( );
                    // Leer el archivo linea por linea
                    do
                    {
                        String[] dato = strLinea.split( "Materia Personal:" );
                        datoMateriaPersonal.add( cambioCaracter(dato[ 1 ]) );
                    } while( ( strLinea = buffer.readLine( ) ) != null && strLinea.contains( "Materia Personal:" ) );

                    objeto.setMateriaPersonal( datoMateriaPersonal );
                }
                else if( strLinea.contains( "Materia General:" ) )
                {
                    ArrayList<String> datoMateriaGeneral = new ArrayList<String>( );
                    // Leer el archivo linea por linea
                    do
                    {
                        String[] dato = strLinea.split( "Materia General:" );
                        datoMateriaGeneral.add( cambioCaracter(dato[ 1 ]) );
                    } while( ( strLinea = buffer.readLine( ) ) != null && strLinea.contains( "Materia General:" ) );

                    objeto.setMateriaGeneral( datoMateriaGeneral );

                }
                else if( strLinea.contains( "Materia Geografica:" ) )
                {
                    ArrayList<String> datoMateriaGeografica = new ArrayList<String>( );
                    // Leer el archivo linea por linea
                    do
                    {
                        String[] dato = strLinea.split( "Materia Geografica:" );
                        datoMateriaGeografica.add( cambioCaracter(dato[ 1 ]) );
                    } while( ( strLinea = buffer.readLine( ) ) != null && strLinea.contains( "Materia Geografica:" ) );

                    objeto.setMateriaGeografica( datoMateriaGeografica );
                }
                else if( strLinea.contains( "Autor Secundario:" ) )
                {
                    ArrayList<String> datoAutorSecundario = new ArrayList<String>( );
                    // Leer el archivo linea por linea
                    do
                    {
                        String[] dato = strLinea.split( "Autor Secundario:" );
                        datoAutorSecundario.add( cambioCaracter(dato[ 1 ]) );
                    } while( ( strLinea = buffer.readLine( ) ) != null && strLinea.contains( "Curso audiencia:" ) );

                    objeto.setAutorSecundario( datoAutorSecundario );
                    fin = true;
                }

            }
            IndexLibrary.saveHashPosition(objeto, objetoBiblioteca.size());
            objetoBiblioteca.add( objeto );
        }
        catch( Exception e )
        { // Catch de excepciones
            System.err.println( "Ocurrio un error: " + e.getMessage( ) );
        }
    }

    public ArrayList<BibliotecaTDO> getObjetoBiblioteca( )
    {
        return objetoBiblioteca;
    }
    
    public String darSiguiente()
    {
    	int postemp=posicion;
    	posicion++;
    	if (posicion == objetoBiblioteca.size())
    		posicion = 0;
    	boolean found = false;
    	int collectionsFound = 0;
    	while (!found && postemp < objetoBiblioteca.size()) {
    		BibliotecaTDO nextBook = objetoBiblioteca.get(postemp);
    		List<List<DBObject>> onCollections = IndexLibrary.getBookByTitleAndAuthor(nextBook);
    		for (List<DBObject> string : onCollections) {
				if (string.size() > 0)
					collectionsFound++;
				if (collectionsFound >= 4)
					found = true;
			}
    		postemp++;
		}
    	
    	return getGsonBiblioteca().toJson(objetoBiblioteca.get(postemp));
    }

    public BibliotecaTDO getAtPosition(int postemp)
    {
    	return objetoBiblioteca.get(postemp);
    }
    
    private String cambioCaracter(String token)
    {
        try
        {
            StringBuffer s = new StringBuffer(token.length());
            
            CharacterIterator it = new StringCharacterIterator(token);
            for (char ch = it.first(); ch != CharacterIterator.DONE; ch = it.next()) {
            	if (ch >= 32 && ch <= 126 || ch == 168 || ch == 173)
            		s.append(ch);
            	else
            		s.append("");
            }
            
            return s.toString().trim();
            
        }
        catch( Exception e )
        {
            e.printStackTrace( );
            return "";
            // TODO: handle exception
        }
    }

	public void borrarvariables() {
		objetoBiblioteca = null;
		try {
			finalize();
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		System.gc();
	}
}
