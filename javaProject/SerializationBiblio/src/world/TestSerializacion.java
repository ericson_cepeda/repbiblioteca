package world;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import controllers.IndexController;

public class TestSerializacion {

	public static final String SPECIALO = "`~!@#$%^&*()_+[]();',./{}|:\"<>?";
	public static final String SPECIAL = "([`\\~!@#$%\\^\\&\\*\\(\\)_\\+\\[\\]\\;\',\\./{}|:\"<>?])";
	
	public static void main(String[] args) {
//		TestSerializacion Test = new TestSerializacion();
		replaceString();
	}
	public TestSerializacion() {
		Serializacion ser = new Serializacion();
		ArrayList<BibliotecaTDO> arreglo = ser.getObjetoBiblioteca( );
		System.out.println(arreglo.size( ));
		System.out.println(IndexController.getStatistics());
		int i = 0;
		for( BibliotecaTDO bibliotecaTDO : arreglo )
        {
            System.out.println(bibliotecaTDO.getAutorPersonal( ));
            System.out.println(bibliotecaTDO.getTitulo( ));
            System.out.println();
            i++;
            if(i>100)
            	break;
        }
	}
	
	public static void replaceString(){
		String deweyFromLocalLibrary = "456.15649684654";
    	Pattern deweyPattern = Pattern.compile("([0-9]+\\.[0-9]{1,3})");
    	Matcher matched = deweyPattern.matcher(deweyFromLocalLibrary);
    	if (matched.find()){
    		deweyFromLocalLibrary = matched.group(1);
    	}
    	System.out.println(deweyFromLocalLibrary);
	}

}
