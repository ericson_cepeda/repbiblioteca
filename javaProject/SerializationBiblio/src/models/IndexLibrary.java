package models;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import world.BibliotecaTDO;
import world.DeweyClassification;
import world.Serializacion;

import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;

import config.MongoConnector;

public class IndexLibrary {
	private static DBCollection getIndexCollection(){
		MongoConnector mc = MongoConnector.getInstance( );
        DB bdConex = mc.getMongoDB( );
        return bdConex.getCollection( MongoConnector.INDEX_COLLECTION );
	}
	
	private static DBCollection getBooksCollection(){
		MongoConnector mc = MongoConnector.getInstance( );
        DB bdConex = mc.getMongoDB( );
        return bdConex.getCollection( MongoConnector.BOOKS_COLLECTION );
	}
	
	public static long countIndexLibrary(){
		DBCollection booksCollection = getIndexCollection();
		return booksCollection.count();
	}
	
    public static void cleanLibaryIndex(){
    	MongoConnector mc = MongoConnector.getInstance( );
        DB bdConex = mc.getMongoDB( );
        DBCollection indexCollection = bdConex.getCollection( MongoConnector.INDEX_COLLECTION );
        indexCollection.drop();
        
        BasicDBObject query = new BasicDBObject();
        query.put("capped", false);
        query.put("size", 0);
        query.put("max", 0);
        
        bdConex.createCollection(MongoConnector.INDEX_COLLECTION, query);
//        BasicDBObject query = new BasicDBObject();
//        query.put("position",  java.util.regex.Pattern.compile(".*"));
//        indexCollection.findAndRemove(query);
    }
    
    public static boolean saveHashPosition(BibliotecaTDO objeto, int currentPosition){
        DBCollection booksCollection = getIndexCollection();
        
        BasicDBObject query = new BasicDBObject();
        query.put("position", ""+currentPosition);
        query.put("title", objeto.getTitulo());
        
        DBObject newTuple = ( DBObject )query;
        booksCollection.insert( newTuple );
        return newTuple.get( "_id" ) != null ? true : false;    	
    }
    
    public static List<DBObject> getIndexBookByTitle(String title){
    	title = title.replaceAll(Serializacion.SPECIAL, "\\\\$1");
        DBCollection indexCollection = getIndexCollection();
        
        BasicDBObject query = new BasicDBObject();
        query.put("title",  java.util.regex.Pattern.compile(".*"+title+".*"));
        
        return indexCollection.find(query).toArray(); 	
    }
    
    public static List<List<DBObject>> getBookByTitleAndAuthor(BibliotecaTDO libraryBook){
    	String title = libraryBook.getTitulo().replaceAll(Serializacion.SPECIAL, "\\\\$1");
    	String autor =  libraryBook.getAutorPersonal().get(0).replaceAll(Serializacion.SPECIAL, "\\\\$1");
        DBCollection indexCollection = getBooksCollection();
        
        List<List<DBObject>> results = new ArrayList<List<DBObject>>();
        
        String[] queries = {"core.metadata.title","arxiv.title","googleDTO.volumeInfo.title","isbnBooks.TitleLong"};
        String[] authorQueries = {"core.metadata.creator","arxiv.authors","googleDTO.volumeInfo.authors","isbnBooks.AuthorsText"};
        int counter = 0;
        for (String string : queries) {
        	BasicDBObject query = new BasicDBObject();
            query.put(string,  java.util.regex.Pattern.compile(".*"+title+".*"));
            List<DBObject> fromCollection = indexCollection.find(query).toArray();
            
            BasicDBObject queryAuthor = new BasicDBObject();
            queryAuthor.put(authorQueries[counter],  java.util.regex.Pattern.compile(".*"+autor+".*"));
            List<DBObject> fromAuthorCollection = indexCollection.find(query).toArray();
            if (fromCollection.size() > 0)
            	results.add(fromCollection);
            else if (fromAuthorCollection.size() > 0){
            	results.add(fromAuthorCollection);
            }
            counter++;
		}
        if (results.size() > 0)
        	return results; 
        return null;
    }
    
    public static BasicDBObject getDeweyCategories(String deweyFromLocalLibrary){
    	Pattern deweyPattern = Pattern.compile("([0-9]+\\.[0-9]+)");
    	Matcher matched = deweyPattern.matcher(deweyFromLocalLibrary);
    	if (matched.find()){
    		deweyFromLocalLibrary = matched.group(1);
    	}
    	
    	return DeweyClassification.categoriasDewey(deweyFromLocalLibrary);
    }
}
