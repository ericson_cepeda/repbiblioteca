$(function() {
	Biblioteca.init();
});

var Biblioteca = {
	viewsURL : "",
	viewsExtension : "",
	isIncompatibleBrowser: false,
	defaultParams : {
		successFunction : null,
		contentType : "json",
		data: {}
	},
	init : function() {
		Biblioteca.quickSearch();
		Biblioteca.viewsExtension = ".jsp";
		Biblioteca.viewsURL = "views/";
		Biblioteca.defaultParams["successFunction"] = Biblioteca.updateElements;
		Biblioteca.intervalStatistics();
		Biblioteca.cleanVariables();
		$('input').click(function(){
			$(this).focus();
		});
	},
	cleanVariables : function() {
		$("#clean_variables").on("click", function(event) {
			event.preventDefault();
			var params = {
				"view" : "cleanVariables",
				"controller" : "index"
			}
			Biblioteca.doRequest(params);
		});
	},
	quickSearch : function() {
		$("#quickSearchForm").on("submit", function(event) {
			event.preventDefault();
			var params = {
				"view" : "getBookByTitle",
				"controller" : "index",
				"data" : $("#quickSearchForm").serialize()
			}
			Biblioteca.doRequest(params);
		});
	},
	intervalStatistics : function() {
		var params = {
			"view" : "getStatistics",
			"controller" : "index"
		};
		//window.setInterval(function() {
			Biblioteca.doRequest(params);
		//}, 5000);
	},
	doRequest : function(params) {
		params = $.extend({}, Biblioteca.defaultParams, params);
		$.ajax({
			type : 'GET',
			url : Biblioteca.viewsURL + params.controller + "/" + params.view
					+ Biblioteca.viewsExtension,
			data : params.data,
			jsonpCallback : 'jsonCallback',
			contentType : "application/" + params.contentType,
			dataType : 'jsonp',
			complete : function(response) {
				params.successFunction(response, params.contentType);
			},
			error : function(e) {

			}
		});
	},
	updateElements : function(response, contentType) {
		switch (contentType) {
		case "json":
			response = $.parseJSON(response["responseText"]);
			for ( var key in response) {
				if (response.hasOwnProperty(key)) {
					$("#" + key).text(response[key]);
				}
			}
			break;
		case "html":
			response = $(response["responseText"]);
			response.children("*").each(
					function() {
						if (Biblioteca.isIncompatibleBrowser == true)
							$("#" + $(this).attr('id')).get(0).innerHtml = $(
									this).get(0).innerHtml;
						else
							$("#" + $(this).attr('id')).html($(this).html());
						$("#" + $(this).attr('id')).change();
//						var currentReplacedId = $j(this).attr('id');
//						if (typeof currentReplacedId == "string"
//								&& currentReplacedId.match("\w*msg")) {
//							Ekarda.Tools.setMessagesTimeout();
//						}
					});
			break;
		default:
			break;
		}

		console.log("GUI updated");

	}
};