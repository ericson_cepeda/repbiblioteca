<%@page contentType="application/html; charset=UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ page language="java" import="java.util.*" pageEncoding="ISO-8859-1"%>
<%@ page import="controllers.IndexController"%>
<%@ page import="comparacion.DtoRespuesta"%>
<%
	ArrayList<DtoRespuesta> analysisResult = IndexController.getBook(request.getParameter( "title" ));
	request.setAttribute("analysisResult", analysisResult);
%>
<response>
<div id="searchedBook" class="center">
					<c:forEach var="respuesta" items="${analysisResult}">
						<h3>${respuesta.getIdHash()}</h3>
						<p>
								<em>${respuesta.getCategoriaSugerias()}</em>
						</p>
						<p>
								${respuesta.getVarStringDeweyRta()}
						</p>
						<p>
								${respuesta.getVarStringBusquedaRta()}
						</p>
						<p>
								${respuesta.getErroresDewey()}
						</p>						
					</c:forEach>
</div>
</response>