$(function() {
	Biblioteca.init();
});

var Biblioteca = {
	serializationURL: "",
	viewsURL : "",
	viewsExtension : "",
	isIncompatibleBrowser: false,
	defaultParams : {
		successFunction : null,
		contentType : "json",
		needsSerial: false,
		data: {}
	},
	init : function() {
		Biblioteca.quickSearch();
		Biblioteca.viewsExtension = ".jsp";
		Biblioteca.viewsURL = "views/";
		Biblioteca.serializationURL = "/serializationbiblio/";
		Biblioteca.defaultParams["successFunction"] = Biblioteca.updateElements;
		Biblioteca.intervalStatistics();
		$('input').click(function(){
			$(this).focus();
		});
	},
	quickSearch : function() {
		$("#quickSearchForm").on("submit", function(event) {
			event.preventDefault();
			var params = {
				"view" : "getBook",
				"controller" : "index",
				"contentType" : "html",
				"data" : $("#quickSearchForm").serialize()
			};
			Biblioteca.doRequest(params);
		});
	},
	intervalStatistics : function() {
		var params = {
			"view" : "getStatistics",
			"controller" : "index",
			"needsSerial": true
		};
		$('#beginSerialization').click(function(){
			params.successFunction = Biblioteca.updateInfoRequest;
			Biblioteca.doRequest(params);
		});
	},
	updateInfoRequest : function(){
		var params = {
				"view" : "getStatistics",
				"controller" : "index",
				"needsSerial": false
			};
		Biblioteca.doRequest(params);
	},
	doRequest : function(params) {
		params = $.extend({}, Biblioteca.defaultParams, params);
		
		url = Biblioteca.viewsURL + params.controller + "/" + params.view
		+ Biblioteca.viewsExtension;
		if (params.needsSerial == true)
			url = Biblioteca.serializationURL + url;
		
		$.ajax({
			type : 'GET',
			url : url,
			data : params.data,
			jsonpCallback : 'jsonCallback',
			contentType : "application/" + params.contentType,
			dataType : 'jsonp',
			complete : function(response) {
				params.successFunction(response, params.contentType);
			},
			error : function(e) {

			}
		});
	},
	updateElements : function(response, contentType) {
		switch (contentType) {
		case "json":
			response = $.parseJSON(response["responseText"]);
			for ( var key in response) {
				if (response.hasOwnProperty(key)) {
					$("#" + key).text(response[key]);
				}
			}
			break;
		case "html":
			response = $(response["responseText"]);
			response.children("*").each(
					function() {
						if (Biblioteca.isIncompatibleBrowser == true)
							$("#" + $(this).attr('id')).get(0).innerHtml = $(
									this).get(0).innerHtml;
						else
							$("#" + $(this).attr('id')).html($(this).html());
						$("#" + $(this).attr('id')).change();
//						var currentReplacedId = $j(this).attr('id');
//						if (typeof currentReplacedId == "string"
//								&& currentReplacedId.match("\w*msg")) {
//							Ekarda.Tools.setMessagesTimeout();
//						}
					});
			break;
		default:
			break;
		}

		console.log("GUI updated");

	}
};