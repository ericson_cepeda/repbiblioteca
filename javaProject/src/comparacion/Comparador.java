package comparacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import models.BasesConstantes;
import models.Books;
import seeds.ClientServicios;
import world.BibliotecaTDO;
import world.Cargador;

import com.google.gson.Gson;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

public class Comparador
{
    private static String servicioUrl = "http://157.253.238.114:8080/serializationbiblio/views/index/";
    private static String servicioMetodo = "getBookByTitle.jsp?callback=jsonCallback&title=";
    private static String servicioMetodoDos = "&_=1351286739326";
    private static ArrayList<DtoRespuesta> rtaLibreria;

    private static Gson gson;
    
    private static Comparador INSTANCE;
    
    public Comparador( )
    {
        rtaLibreria= new ArrayList<DtoRespuesta>( );
        gson = new Gson( );

    }

    public static ArrayList<DtoRespuesta> getRtaLibreria( )
    {
        return rtaLibreria;
    }
    public static Comparador getInstance(){
        if (INSTANCE == null)
            INSTANCE = new Comparador( );
        return INSTANCE;
    }
    
    public List<Books> covertirAListaBooks( List<DBObject> listaDb )
    {
        Gson gson = new Gson( );
        List<Books> listaRespuesta = new ArrayList<Books>( );

        for( DBObject dbObject : listaDb )
        {
            Books libros = gson.fromJson( dbObject.toString( ), Books.class );
            listaRespuesta.add( libros );
        }

        return listaRespuesta;
    }

    public static void resultadoBusqueda( String titulo )
    {

        String servicio = servicioUrl + servicioMetodo + titulo + servicioMetodoDos;
        String entrada = "";
        try
        {
            entrada = ClientServicios.getHtml( servicio );
            if( entrada.equals( "-1" ) )
            {
                throw new Exception( "Error en llamada del servicio getBookByTitle" );
            }
        }
        catch( Exception e )
        {
            e.printStackTrace( );
        }
        
        DBObject dbObject = (DBObject) JSON.parse( entrada );
        Set<String> set = dbObject.keySet( );
        analizarLibros (set, dbObject);
    }
    
    public static void analizarLibros(Set<String> set, DBObject dbObject)
    {
        for( String setMongo : set )
        {
            DtoRespuesta prmAnalisisLibro = new DtoRespuesta( );
            
            prmAnalisisLibro.setIdHash( Integer.parseInt( setMongo ) );
            
            DBObject objetos = ( DBObject )dbObject.get( setMongo );
            
            DBObject deweyInfo = ( DBObject )objetos.get( "deweyInfo" );
            
            DBObject categories = ( DBObject )deweyInfo.get( "categories" );
            
            DBObject errors = ( DBObject )deweyInfo.get( "errors" );
            
            if( errors !=null  )
            {                
                Set<String> setErrors = errors.keySet( );
                ArrayList<String> erroresArray = new ArrayList<String>( );
                for( String errores : setErrors )
                {
                    erroresArray.add( errors.get( errores).toString( ) );
                }
                prmAnalisisLibro.setErroresDewey( erroresArray );
            }
            
            if(categories == null )
            {
                prmAnalisisLibro.setVarStringDeweyRta( ConstantesRespuestas.ERROR_DEWEY );
            }
            else
            {
                analizarDewey( objetos , prmAnalisisLibro );
            }
            rtaLibreria.add( prmAnalisisLibro );
        }
    }
    
    private static void analizarDewey( DBObject objetos, DtoRespuesta prmAnalisisLibro )
    {
        DBObject libraryBook = ( DBObject )objetos.get( "libraryBook" );
        DBObject deweyInfo = ( DBObject )objetos.get( "deweyInfo" );
        DBObject categories = ( DBObject )deweyInfo.get( "categories" );
        
        Set<String> setErrors = categories.keySet( );
        ArrayList<String> categoriasArray = new ArrayList<String>( );
        for( String errores : setErrors )
        {
            categoriasArray.add( categories.get( errores).toString( ) );
        }
        prmAnalisisLibro.setCategoriaSugerias( categoriasArray );
        
        DBObject collectionBooks = ( DBObject )objetos.get( "collectionBooks" );
        
        if( collectionBooks != null )
        {
            Set<String> set = collectionBooks.keySet( );
            
              for( String books : set )
              {
                  Books libro = gson.fromJson( books, Books.class );

                  switch( Integer.parseInt( libro.getColeccion( ) ) )
                  {
                      case BasesConstantes.COREINT:
                          break;
      
                      case BasesConstantes.ARXIVINT:
                          break;
      
                      case BasesConstantes.GOOGLEBOOKSINT:
                          break;
      
                      case BasesConstantes.OPENLIBRARYINT:
                          break;
      
                      case BasesConstantes.ISBNDTOINT:
                          break;
                      default:
                          break;
                  }
              }
        }else
        {
            prmAnalisisLibro.setVarStringBusquedaRta( ConstantesRespuestas.ERROR_BUSQUEDA );
            try
            {
                Cargador cargador;
                cargador = new Cargador( );
                BibliotecaTDO libro = gson.fromJson( libraryBook.toString( ), BibliotecaTDO.class );
                for( String autor : libro.getAutorPersonal( ) )
                {
                    cargador.llamarArxiv( autor );
                }
            }
            catch( Exception e )
            {
                e.printStackTrace();
            }
        }
    }

//    public void analizarBusqueda( List<Books> listaBooksMongo )
//    {
//        for( Books books : listaBooksMongo )
//        {
//            switch( Integer.parseInt( books.getColeccion( ) ) )
//            {
//                case BasesConstantes.COREINT:
//                    varAnalizadorCore.inicializar( books, listaBooksMongo, varLibroBiblioteca );
//                    
//                    break;
//
//                case BasesConstantes.ARXIVINT:
//                    varAnalizadorArxiv.inicializar( books, listaBooksMongo, varLibroBiblioteca );
//                    break;
//
//                case BasesConstantes.GOOGLEBOOKSINT:
//        
//                    break;
//
//                case BasesConstantes.OPENLIBRARYINT:
//                    
//                    break;
//
//                case BasesConstantes.ISBNDTOINT:
//                    
//                    break;
//
//                default:
//                    break;
//            }
//        }
//    }

}
