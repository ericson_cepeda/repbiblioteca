package comparacion;

import java.util.ArrayList;

public class DtoRespuesta
{
    private int idHash;
    
    private ArrayList<String> erroresDewey;
    
    private ArrayList<String> categoriaSugerias;
    
    private String varStringDeweyRta;
    
    private String varStringBusquedaRta;
    
    public DtoRespuesta( )
    {
        
    }
    
    public ArrayList<String> getErroresDewey( )
    {
        return erroresDewey;
    }

    public void setErroresDewey( ArrayList<String> erroresDewey )
    {
        this.erroresDewey = erroresDewey;
    }

    public ArrayList<String> getCategoriaSugerias( )
    {
        return categoriaSugerias;
    }

    public void setCategoriaSugerias( ArrayList<String> categoriaSugerias )
    {
        this.categoriaSugerias = categoriaSugerias;
    }

    public int getIdHash( )
    {
        return idHash;
    }

    public void setIdHash( int idHash )
    {
        this.idHash = idHash;
    }

    public String getVarStringDeweyRta( )
    {
        return varStringDeweyRta;
    }

    public void setVarStringDeweyRta( String varStringDeweyRta )
    {
        this.varStringDeweyRta = varStringDeweyRta;
    }

    public String getVarStringBusquedaRta( )
    {
        return varStringBusquedaRta;
    }

    public void setVarStringBusquedaRta( String varStringBusquedaRta )
    {
        this.varStringBusquedaRta = varStringBusquedaRta;
    }
    
    
}
