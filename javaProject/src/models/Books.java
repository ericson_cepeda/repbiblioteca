package models;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBObject;
import com.mongodb.util.JSON;

import config.MongoConnector;
import dto.ArXivDTO;
import dto.CoreDTO;
import dto.GoogleDTO;
import dto.IsbnBdDTO;
import dto.OpenDTO;

public class Books
{
    private String coleccion;
    private CoreDTO core;
    private ArXivDTO arxiv;
    private GoogleDTO googleDTO;
    private IsbnBdDTO isbnBooks;
    private OpenDTO openLibrary;

    public Books( String coleccion )
    {
        int cambio = Integer.parseInt( coleccion );
        switch( cambio )
        {
            case BasesConstantes.COREINT:
                core = new CoreDTO( );
                break;
            case BasesConstantes.ARXIVINT:
                arxiv = new ArXivDTO( );
                break;
            case BasesConstantes.GOOGLEBOOKSINT:
                googleDTO = new GoogleDTO( );
                break;
            case BasesConstantes.OPENLIBRARYINT:
                openLibrary = new OpenDTO( );
                break;
            case BasesConstantes.ISBNDTOINT:
                isbnBooks = new IsbnBdDTO( );
           break;
            default:
                break;
        }

    }   
    

    public OpenDTO getOpenLibrary( )
    {
        return openLibrary;
    }

    public void setOpenLibrary( OpenDTO openLibrary )
    {
        this.openLibrary = openLibrary;
    }

    public String getColeccion( )
    {
        return coleccion;
    }

    public void setColeccion( String coleccion )
    {
        this.coleccion = coleccion;
    }

    public IsbnBdDTO getIsbnBooks( )
    {
        return isbnBooks;
    }

    public void setIsbnBooks( IsbnBdDTO isbnBooks )
    {
        this.isbnBooks = isbnBooks;
    }

    public GoogleDTO getGoogleBooks( )
    {
        return googleDTO;
    }

    public void setGoogleBooks( GoogleDTO googleBooks )
    {
        this.googleDTO = googleBooks;
    }

    public CoreDTO getCore( )
    {
        return core;
    }

    public void setCore( CoreDTO core )
    {
        this.core = core;
    }

    public ArXivDTO getArxiv( )
    {
        return arxiv;
    }

    public void setArxiv( ArXivDTO arxiv )
    {
        this.arxiv = arxiv;
    }

    public static boolean saveBook( Books newBook )
    {
        MongoConnector mc = MongoConnector.getInstance( );
        DB bdConex = mc.getMongoDB( );
        DBCollection booksCollection = bdConex.getCollection( MongoConnector.BOOKS_COLLECTION );
        Gson json = new Gson( );
        Object obj = JSON.parse( json.toJson( newBook ) );
        DBObject objeto = ( DBObject )obj;
        booksCollection.insert( objeto );
        return objeto.get( "_id" ) != null ? true : false;
    }

    public static List<JSON> libros( BasicDBObject query )
    {
        MongoConnector mc = MongoConnector.getInstance( );
        DB bdConex = mc.getMongoDB( );
        DBCollection booksCollection = bdConex.getCollection( MongoConnector.BOOKS_COLLECTION );
        List<JSON> primerosCincuenta = new ArrayList<JSON>( );
        List<DBObject> found = booksCollection.find( query ).limit( 50 ).toArray( );
        for( Iterator<DBObject> iterator = found.iterator( ); iterator.hasNext( ); )
        {
            DBObject dbObject = iterator.next( );
            primerosCincuenta.add( ( JSON )JSON.parse( dbObject.toString( ) ) );
        }
        return primerosCincuenta;
    }

    public static long cantidadLibros( )
    {
        MongoConnector mc = MongoConnector.getInstance( );
        DB bdConex = mc.getMongoDB( );
        DBCollection booksCollection = bdConex.getCollection( MongoConnector.BOOKS_COLLECTION );
        return booksCollection.count( );
    }
    
    public static JsonObject getStatistics(){
        MongoConnector mc = MongoConnector.getInstance( );
        DB bdConex = mc.getMongoDB( );
        DBCollection booksCollection = bdConex.getCollection( MongoConnector.BOOKS_COLLECTION );
        
        BasicDBObject query = new BasicDBObject();
        query.put("coleccion", "");
        
        JsonObject response = new JsonObject();
        
        String[] collections = {BasesConstantes.ARXIV,BasesConstantes.CORE,BasesConstantes.GOOGLEBOOKS,BasesConstantes.ISBNDTO,BasesConstantes.OPENLIBRARY};
        for( int i = 0; i < collections.length; i++ )
        {
            query.put("coleccion", collections[i]);
            response.addProperty( collections[i], booksCollection.count( query ) );
        }
        
        return response;
    }

}
