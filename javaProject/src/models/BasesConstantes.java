package models;

public class BasesConstantes
{
    public static String CORE = "1";
    public static String ARXIV = "2";
    public static String GOOGLEBOOKS = "3";
    public static String OPENLIBRARY = "4";
    public static String ISBNDTO = "5";
    
    public static final int COREINT = 1;
    public static final int ARXIVINT = 2;
    public static final int GOOGLEBOOKSINT = 3;
    public static final int OPENLIBRARYINT = 4;
    public static final int ISBNDTOINT = 5;
}
