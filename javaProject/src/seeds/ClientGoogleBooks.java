package seeds;

import java.util.ArrayList;

import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.json.simple.JSONValue;

import com.google.gson.Gson;

import dto.GoogleDTO;
import dto.GoogleVolumenDTO;

import models.BasesConstantes;
import models.Books;
import models.GoogleIndustryId;


public class ClientGoogleBooks
{
    public static ArrayList<Books> buscarEnBdGoogle( String busqueda )
    {
        Gson gson = new Gson( );
        String getJson = ServiciosConstantes.GOOGLEBD + busqueda + ServiciosConstantes.GOOGLEKEY;
        String jsonRta = "";
        try
        {
            jsonRta = ClientServicios.getHtml( getJson );

        }
        catch( Exception e )
        {
            e.printStackTrace( );
            jsonRta = "";
        }
        ArrayList<Books> librosBooks = analizdorGoogle( jsonRta );
        return librosBooks;
    }
    
    public static ArrayList<Books> analizdorGoogle(String libro)
    {
        ArrayList<Books> librosJson = new ArrayList<Books>( );
        Object obj=JSONValue.parse(libro);
        JSONObject entryInformation = JSONObject.fromObject( obj );
        JSONArray items = JSONArray.fromObject( entryInformation.get( "items" ) );
        
        for (Object rtaRecord : items) 
        {
            Books libros = new Books( BasesConstantes.GOOGLEBOOKS  );
            JSONObject cmRecord = JSONObject.fromObject( rtaRecord ) ;
            GoogleDTO libroJson = generarBookLibro(cmRecord);
            libros.setColeccion( BasesConstantes.GOOGLEBOOKS );
            libros.setGoogleBooks( libroJson );
            //Books.saveBook( libros );
            librosJson.add( libros );
        }
        
        return librosJson;
    }
    
    public static GoogleDTO generarBookLibro(JSONObject libroJson)
    {
        GoogleDTO objetoLibro = new GoogleDTO( );
        String objetokind =  libroJson.getString( "kind" );
        objetoLibro.setKind( objetokind );
        String objetoEtag = libroJson.getString( "etag" );
        objetoLibro.setEtag( objetoEtag );
        String objetoSelfLink =  libroJson.getString( "selfLink" );
        objetoLibro.setSelfLink( objetoSelfLink );
        JSONObject objetoRecord = JSONObject.fromObject( libroJson.get( "volumeInfo" ) );
        
        GoogleVolumenDTO volumenGoogle = crearVolumenGoogle( objetoRecord );
        
        objetoLibro.setVolumengoogleDTO( volumenGoogle );

        return objetoLibro;
    }
    
    public static GoogleVolumenDTO crearVolumenGoogle( JSONObject objetoRecord )
    {
        GoogleVolumenDTO volumenGoogle = new GoogleVolumenDTO( );
        
        try
        {
            String objetoPageCount =  objetoRecord.getString( "pageCount" );
            volumenGoogle.setPageCount( objetoPageCount );

        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloCategories = new ArrayList<String>( );
            JSONArray categories =  JSONArray.fromObject(objetoRecord.get( "categories" ));
            for (Object rtaCategories : categories) 
            {
                arregloCategories.add( rtaCategories.toString( ) );
            }
            volumenGoogle.setCategories( arregloCategories );

        }
        catch( Exception e ){ }
        
        try
        {
            String objetoAverageRating = objetoRecord.getString( "averageRating" );
            volumenGoogle.setAverageRating( objetoAverageRating );
        }
        catch( Exception e ){ }
        
        try
        {
            String objetoInfoLink = objetoRecord.getString( "infoLink" );
            volumenGoogle.setInfoLink( objetoInfoLink );
        }
        catch( Exception e ){ }
        
        try
        {
            String objetoPrintType =  objetoRecord.getString( "printType" );
            volumenGoogle.setPrintType( objetoPrintType );
        }
        catch( Exception e ){ }
        
        try
        {
            String objetoPublisher =  objetoRecord.getString( "publisher" );
            volumenGoogle.setPublisher( objetoPublisher );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloAuthores = new ArrayList<String>( );
            JSONArray authors =  JSONArray.fromObject(objetoRecord.get( "authors" ));
            for (Object rtaAuthors : authors) 
            {
                arregloAuthores.add( rtaAuthors.toString( ) );
            }
            volumenGoogle.setAuthors( arregloAuthores );
        }
        catch( Exception e ){ }
        
        try
        {
            String objetoCanonicalVolumeLink =  objetoRecord.getString( "canonicalVolumeLink" );
            volumenGoogle.setCanonicalVolumeLink( objetoCanonicalVolumeLink );
        }
        catch( Exception e ){ }
        
        try
        {            
            String objetoTitle = objetoRecord.getString( "title" );
            volumenGoogle.setTitle( objetoTitle );
        }
        catch( Exception e ){ }

        try
        {
            
            String objetoPreviewLink =  objetoRecord.getString( "previewLink" );
            volumenGoogle.setPreviewLink( objetoPreviewLink );            
        }
        catch( Exception e ){ }

        try
        {
            String objetoDescription =  objetoRecord.getString( "description" );
            volumenGoogle.setDescription( objetoDescription );
        }
        catch( Exception e ){ }
        
        try
        {
            String objetoRatingsCount =  objetoRecord.getString( "ratingsCount" );
            volumenGoogle.setRatingsCount( objetoRatingsCount );
        }
        catch( Exception e ){ }
        
//        try
//        {
//            String objetoImageLinks =  objetoRecord.getString( "imageLinks" );
//            volumenGoogle.
//            System.out.println(objetoImageLinks);
//        }
//        catch( Exception e ){ }

        try
        {
            String objetoSubtitle =  objetoRecord.getString( "subtitle" );
            volumenGoogle.setSubtitle( objetoSubtitle );
        }
        catch( Exception e ){ }
        
        try
        {
            String objetoContentVersion =  objetoRecord.getString( "contentVersion" );
            volumenGoogle.setContentVersion( objetoContentVersion );
        }
        catch( Exception e ){ }
        
        try
        {
            String objetoLanguage = objetoRecord.getString( "language" );
            volumenGoogle.setLanguage( objetoLanguage );
        }
        catch( Exception e ){ }

        try
        {
            String objetoPublishedDate =  objetoRecord.getString( "publishedDate" );
            volumenGoogle.setPublishedDate( objetoPublishedDate );
        }
        catch( Exception e ){ }
        
        try
        {         
            JSONArray industryIdentifiers =  JSONArray.fromObject(objetoRecord.get( "industryIdentifiers" ));
            ArrayList<GoogleIndustryId> annregloIndustru = new ArrayList<GoogleIndustryId>( );
            for (Object rtaIndustryIdentifiers : industryIdentifiers) 
            {
                GoogleIndustryId industry = new GoogleIndustryId( );

                JSONObject cmIndustryIdentifiers = JSONObject.fromObject( rtaIndustryIdentifiers ) ;
                industry.setIdentifier( cmIndustryIdentifiers.getString( "identifier" ) );
                industry.setType( cmIndustryIdentifiers.getString( "type" ) );
                annregloIndustru.add( industry );
            }
            volumenGoogle.setIndustryIdentifiers( annregloIndustru );
        }
        catch( Exception e ){ }
        
        return volumenGoogle;
    }
    
}
