package seeds;

import java.io.StringWriter;
import java.net.URL;
import java.net.URLConnection;
import java.util.Scanner;

import javax.xml.namespace.QName;
import javax.xml.rpc.ServiceException;
import javax.xml.rpc.ServiceFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.ws.Service;

import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import com.sun.jersey.api.client.Client;
//import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;


public class ClientServicios
{
    public static String getHtml( String url ) throws Exception
    {
        String content = null;
        URLConnection connection = null;
        try {
          connection =  new URL(url).openConnection();
          connection.addRequestProperty("User-Agent", "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)");
          Scanner scanner = new Scanner(connection.getInputStream());
          scanner.useDelimiter("\\Z");
          content = scanner.next();
        }catch ( Exception ex ) {
            throw new Exception( ex.getMessage( ) );
        }
        return content;
    }
    
    public static Node getNode( String tagName, NodeList nodes )
    {
        for( int x = 0; x < nodes.getLength( ); x++ )
        {
            Node node = nodes.item( x );
            if( node.getNodeName( ).equalsIgnoreCase( tagName ) )
            {
                return node;
            }
        }

        return null;
    }

    public static String getNodeValue( Node node )
    {
        NodeList childNodes = node.getChildNodes( );
        for( int x = 0; x < childNodes.getLength( ); x++ )
        {
            Node data = childNodes.item( x );
            if( data.getNodeType( ) == Node.TEXT_NODE )
                return data.getNodeValue( );
        }
        return "";
    }

    public static String getNodeValue( String tagName, NodeList nodes )
    {
        for( int x = 0; x < nodes.getLength( ); x++ )
        {
            Node node = nodes.item( x );
            if( node.getNodeName( ).equalsIgnoreCase( tagName ) )
            {
                NodeList childNodes = node.getChildNodes( );
                for( int y = 0; y < childNodes.getLength( ); y++ )
                {
                    Node data = childNodes.item( y );
                    if( data.getNodeType( ) == Node.TEXT_NODE )
                        return data.getNodeValue( );
                }
            }
        }
        return "";
    }

    public static String getNodeAttr( String attrName, Node node )
    {
        NamedNodeMap attrs = node.getAttributes( );
        for( int y = 0; y < attrs.getLength( ); y++ )
        {
            Node attr = attrs.item( y );
            if( attr.getNodeName( ).equalsIgnoreCase( attrName ) )
            {
                return attr.getNodeValue( );
            }
        }
        return "";
    }

    public static String getNodeAttr( String tagName, String attrName, NodeList nodes )
    {
        for( int x = 0; x < nodes.getLength( ); x++ )
        {
            Node node = nodes.item( x );
            if( node.getNodeName( ).equalsIgnoreCase( tagName ) )
            {
                NodeList childNodes = node.getChildNodes( );
                for( int y = 0; y < childNodes.getLength( ); y++ )
                {
                    Node data = childNodes.item( y );
                    if( data.getNodeType( ) == Node.ATTRIBUTE_NODE )
                    {
                        if( data.getNodeName( ).equalsIgnoreCase( attrName ) )
                            return data.getNodeValue( );
                    }
                }
            }
        }

        return "";
    }

    public static String nodeToString( Node node )
    {
        StringWriter sw = new StringWriter( );
        try
        {
            Transformer t = TransformerFactory.newInstance( ).newTransformer( );
            t.setOutputProperty( OutputKeys.OMIT_XML_DECLARATION, "yes" );
            t.setOutputProperty( OutputKeys.INDENT, "yes" );
            t.transform( new DOMSource( node ), new StreamResult( sw ) );
        }
        catch( TransformerException te )
        {
            System.out.println( "nodeToString Transformer Exception" );
        }
        return sw.toString( );
    }
}
