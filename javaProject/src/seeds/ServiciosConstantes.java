package seeds;

public class ServiciosConstantes
{
    public static String ARXIV= "http://export.arxiv.org/api/query?";
    public static String ARXIVAUTOR = "http://export.arxiv.org/api/query?search_query=au:";
    
    public static String COREBD = "http://core.kmi.open.ac.uk/api/search/";
    public static String COREJSON = "?format=json";
    public static String COREKEY = "&api_key=1A3yVU1dSixA1AyHixsRAbWxUQCP28JM";
    
    public static String GOOGLEBD = "https://www.googleapis.com/books/v1/volumes?q=";
    public static String GOOGLEKEY = ":keyes&key=AIzaSyCwSLdzcWJSjIx-gh3CGwzku_-ncsjiaxs";
    
    public static String ISBNBD = "http://isbndb.com/api/books.xml?access_key=";
    public static String ISBNKEY = "HDEEZIWQ";
    public static String ISBN_TITULO = "&results=details&index1=title&value1=";
    public static String ISBN_ISBN = "&results=details&index1=isbn&value1=";
    public static String ISBN_FULL = "&results=details&index1=full&value1=";
    public static String ISBN_PAGE = "&page_number=";
    
    public static String OPEN_ISBN = "http://openlibrary.org/api/books?bibkeys=ISBN:";
    public static String OPEN_DATA = "&jscmd=data&format=json";
}
