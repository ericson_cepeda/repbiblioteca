package seeds;

import java.io.StringReader;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import models.BasesConstantes;
import models.Books;
import net.sf.json.JSONArray;

import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.gson.Gson;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;

import dto.IsbnBdDTO;
import dto.IsbnDetailsDTO;

public class ClientISBNDB
{

    public static String buscarEnBdISBN_Full( String busqueda )
    {
        Gson gson = new Gson( );
        ArrayList<Books> libros = new ArrayList<Books>( );
        String getJson = ServiciosConstantes.ISBNBD + ServiciosConstantes.ISBNKEY + ServiciosConstantes.ISBN_FULL + busqueda;
        String jsonRta = "";
        try
        {
            System.out.println( "Entro" );
            System.out.println( getJson );

            jsonRta = ClientServicios.getHtml( getJson );

            Pattern attributePattern = Pattern.compile( "<BookList total_results=\"([0-9]+)\" page_size=\"([0-9]+)\" page_number=\"([0-9]+)\" shown_results=\"([0-9]+)\"", Pattern.MULTILINE );
            Matcher codigo = attributePattern.matcher( jsonRta );
            
            if(codigo.find( )){
                int totaResults = Integer.parseInt( codigo.group( 1 ) );
                int pageSize = Integer.parseInt( codigo.group( 2 ) );
                int pageNumber = Integer.parseInt( codigo.group( 3 ) );
                int shownResults = Integer.parseInt( codigo.group( 4 ) );
                for( int i = pageNumber; i<= totaResults/pageSize && shownResults > 0; i++ )
                {
                    libros.addAll( analizdorXmlIsbn( jsonRta ));
                    jsonRta = ClientServicios.getHtml( getJson + ServiciosConstantes.ISBN_PAGE + i+1 );
                    codigo = attributePattern.matcher( jsonRta );
                    if(codigo.find( ))
                        shownResults = Integer.parseInt( codigo.group( 4 ) );
                }                
            }
            
        }
        catch( Exception e )
        {
            e.printStackTrace( );
            jsonRta = "";

        }
        return gson.toJson( libros );
    }

    public static String buscarEnBdISBN_ISBN( String busqueda )
    {
        Gson gson = new Gson( );
        ArrayList<Books> libros = new ArrayList<Books>( );
        String getJson = ServiciosConstantes.ISBNBD + ServiciosConstantes.ISBNKEY + ServiciosConstantes.ISBN_ISBN + busqueda;
        String jsonRta = "";
        try
        {
            jsonRta = ClientServicios.getHtml( getJson );

        }
        catch( Exception e )
        {
            e.printStackTrace( );
            jsonRta = "";

        }
        if( jsonRta != "" )
        {
            libros = analizdorXmlIsbn( jsonRta );
        }

        return gson.toJson( libros );
    }

    public static String buscarEnBdISBN_Titulo( String busqueda )
    {
        Gson gson = new Gson( );
        ArrayList<Books> libros = new ArrayList<Books>( );
        String getJson = ServiciosConstantes.ISBNBD + ServiciosConstantes.ISBNKEY + ServiciosConstantes.ISBN_TITULO + busqueda;
        String jsonRta = "";
        try
        {
            jsonRta = ClientServicios.getHtml( getJson );

        }
        catch( Exception e )
        {
            e.printStackTrace( );
            jsonRta = "";

        }
        if( jsonRta != "" )
        {
            libros = analizdorXmlIsbn( jsonRta );
        }

        return gson.toJson( libros );
    }

    public static ArrayList<Books> analizdorXmlIsbn( String entrada )
    {
        ArrayList<Books> librosArreli = new ArrayList<Books>( );
        
        /*
         * Parser for single properties
         */
        DOMParser parser = new DOMParser( );

        InputSource is = new InputSource( );
        is.setCharacterStream( new StringReader( entrada ) );

        try
        {
            parser.parse( is );
        }
        catch( Exception e )
        {
            e.printStackTrace();
            return librosArreli;
        }
        
        Document doc = parser.getDocument( );

        // Get the document's root XML node
        NodeList root = doc.getElementsByTagName( "BookData" );
        for( int x = 0; x < root.getLength( ); x++ ){
            Books libro = new Books( BasesConstantes.ISBNDTO );
            libro.setColeccion( BasesConstantes.ISBNDTO );
            
            IsbnBdDTO isbnBd = new IsbnBdDTO( );
            Node exec = root.item( x );
            String isbn = ClientServicios.getNodeAttr("isbn", exec);
            String isbn13 = ClientServicios.getNodeAttr("isbn13", exec);
            String bookId = ClientServicios.getNodeAttr("book_id", exec);
            isbnBd.setIsbn( isbn );
            isbnBd.setIsbn13( isbn13 );
            isbnBd.setBookId( bookId );
            
            NodeList nodes = exec.getChildNodes( );
            
            String title = ClientServicios.getNodeValue( "Title", nodes );
            String titleLong = ClientServicios.getNodeValue( "TitleLong", nodes );
            String authorsText = ClientServicios.getNodeValue( "AuthorsText", nodes );
            String publisherText = ClientServicios.getNodeValue( "PublisherText", nodes );
            isbnBd.setTitle( title );
            isbnBd.setTitleLong( titleLong );
            isbnBd.setAuthorsText( authorsText );
            isbnBd.setPublisherText( publisherText );
            
            Node detailsNode = ClientServicios.getNode( "Details", nodes );
            IsbnDetailsDTO details = new IsbnDetailsDTO( );
            
            String change_time = ClientServicios.getNodeAttr("change_time", detailsNode);
            String price_time = ClientServicios.getNodeAttr("price_time", detailsNode);
            String edition_info = ClientServicios.getNodeAttr("edition_info", detailsNode);
            String language = ClientServicios.getNodeAttr("language", detailsNode);
            String physical_description_text = ClientServicios.getNodeAttr("physical_description_text", detailsNode);
            String lcc_number = ClientServicios.getNodeAttr("lcc_number", detailsNode);
            String dewey_decimal_normalized = ClientServicios.getNodeAttr("dewey_decimal_normalized", detailsNode);
            String dewey_decimal = ClientServicios.getNodeAttr("dewey_decimal", detailsNode);
            details.setChange_time( change_time );
            details.setPrice_time( price_time );
            details.setEdition_info( edition_info );
            details.setLanguage( language );
            details.setPhysical_description_text( physical_description_text );
            details.setLcc_number( lcc_number );
            details.setDewey_decimal_normalized( dewey_decimal_normalized );
            details.setDewey_decimal( dewey_decimal );
            
            isbnBd.setDetails( details );
            libro.setIsbnBooks( isbnBd );
            
            Books.saveBook( libro );
            librosArreli.add( libro );
        }
        return librosArreli;
    }

    public JSONArray darNueroJsonArray( )
    {
        return new JSONArray( );
    }

}
