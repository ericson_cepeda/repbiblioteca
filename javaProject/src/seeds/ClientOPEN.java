package seeds;

import java.util.ArrayList;

import models.BasesConstantes;
import models.Books;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.json.simple.JSONValue;
import com.google.gson.Gson;
import dto.OpenDTO;
import dto.Open_Author_DTO;
import dto.Open_Table_DTO;
import dto.Open_link_DTO;

public class ClientOPEN
{
    
    public static  String buscarEnBdOpen(String busqueda)
        {
            Gson gson = new Gson( );
            Books libros = null;
            String getJson = ServiciosConstantes.OPEN_ISBN + busqueda  + ServiciosConstantes.OPEN_DATA ;
            String jsonRta="";
            try
            {
                 jsonRta= ClientServicios.getHtml( getJson );
                
            }
            catch( Exception e )
            {
                e.printStackTrace( );
                jsonRta="";

            }
            if( jsonRta!="" )
            {
                if( !jsonRta.contentEquals( "{}" ) )
                {                    
                    libros = analizdorJsonOpen(jsonRta , busqueda);  
                }
            }
            if( libros!=null )
            {
                
                return gson.toJson( libros );
            }else
                return "";
        }
    
    public static Books analizdorJsonOpen(String entrada , String busqueda)
    {
        Books libros = new Books( BasesConstantes.OPENLIBRARY );
        Object obj=JSONValue.parse(entrada);
        JSONObject entryInformation = JSONObject.fromObject( obj );
        JSONObject records =  JSONObject.fromObject(entryInformation.get( "ISBN:"+busqueda ));
        OpenDTO rta =generarBookLibro(records, busqueda);
        libros.setColeccion( BasesConstantes.OPENLIBRARY );
        libros.setOpenLibrary( rta );
        Books.saveBook( libros );
        System.out.println(libros);
        return libros;
    }
    
    public static OpenDTO generarBookLibro(JSONObject varJsonObject, String busqueda)
    {
        OpenDTO objetoLibro = new OpenDTO( );
        
        objetoLibro.setISBN( busqueda );
        
        objetoLibro.setTitle( varJsonObject.getString( "title" ) );
        try
        {
            JSONObject classifications =  JSONObject.fromObject(varJsonObject.get( "classifications"));
            objetoLibro.setDewey_decimal_class( classifications.getString( "dewey_decimal_class" ) );
            objetoLibro.setLc_classifications( classifications.getString( "lc_classifications" ) );
        }catch( Exception e ){ }
        
        try
        {
            ArrayList<Open_Author_DTO> arregloOpen_Author_DTO = new ArrayList<Open_Author_DTO>( );
            JSONArray authors =  JSONArray.fromObject(varJsonObject.get( "authors" ));
            for (Object rtaAuthors : authors) 
            {
                Open_Author_DTO tabla = new Open_Author_DTO( );
                JSONObject entryInformation = JSONObject.fromObject( rtaAuthors );
                tabla.setUrl( entryInformation.getString( "url" ) );
                tabla.setName( entryInformation.getString( "name" ) );
    
                arregloOpen_Author_DTO.add( tabla );
            }
            objetoLibro.setAuthor( arregloOpen_Author_DTO );
        }catch( Exception e ){ }
        
        try
        {
            ArrayList<Open_Table_DTO> arregloTable_of_contents = new ArrayList<Open_Table_DTO>( );
            JSONArray table_of_contents =  JSONArray.fromObject(varJsonObject.get( "table_of_contents" ));
            for (Object rtaTable_of_contents : table_of_contents) 
            {
                Open_Table_DTO tabla = new Open_Table_DTO( );
                JSONObject entryInformation = JSONObject.fromObject( rtaTable_of_contents );
                tabla.setTitle( entryInformation.getString( "title" ) );
                tabla.setLevel( entryInformation.getString( "level" ) );
                tabla.setPagenum( entryInformation.getString( "pagenum" ) );
                tabla.setLabel( entryInformation.getString( "label" ) );
                arregloTable_of_contents.add( tabla );
            }
            objetoLibro.setTable_of_contents( arregloTable_of_contents );
        }catch( Exception e ){ }
        
        try
        {
            ArrayList<Open_link_DTO> arregloLinks = new ArrayList<Open_link_DTO>( );
            JSONArray links =  JSONArray.fromObject(varJsonObject.get( "links" ));
            for (Object rtaTable_of_contents : links) 
            {
                Open_link_DTO tabla = new Open_link_DTO( );
                JSONObject entryInformation = JSONObject.fromObject( rtaTable_of_contents );
                tabla.setUrl( entryInformation.getString( "url" ) );
                tabla.setTitle( entryInformation.getString( "title" ) );
                arregloLinks.add( tabla );
            }
            objetoLibro.setLinks( arregloLinks );
        }catch( Exception e ){ }
        
        JSONObject identifiers =  JSONObject.fromObject(varJsonObject.get( "identifiers"));
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "isbn_13" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setIsbn_13( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "isbn_10" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setIsbn_10( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "openlibrary" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setOpenlibrary ( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "google" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setGoogle( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "goodreads" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setGoodreads( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "librarything" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setLibrarything( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "oclc" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setOclc( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "lccn" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setLccn( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
        
        try
        {
            ArrayList<String> arregloRtaIsbn_13 = new ArrayList<String>( );
            JSONArray isbn_13 =  JSONArray.fromObject(identifiers.get( "amazon" ));
            for (Object rtaIsbn_13 : isbn_13) 
            {
                arregloRtaIsbn_13.add( rtaIsbn_13.toString( ) );
            }
            objetoLibro.setAmazon( arregloRtaIsbn_13 );
        }
        catch( Exception e ){ }
           
        return objetoLibro;
    }
    
    public static JSONArray darNueroJsonArray()
    {
        return new JSONArray( );
    }
    

}
