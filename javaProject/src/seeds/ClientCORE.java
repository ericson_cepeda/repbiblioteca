package seeds;

import java.util.ArrayList;

import models.BasesConstantes;
import models.Books;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.json.simple.JSONValue;

import com.google.gson.Gson;

import dto.CoreDTO;
import dto.HeaderDTO;
import dto.MetadataDTO;

public class ClientCORE
{
    
    public static  String buscarEnBdCore(String busqueda)
        {
            Gson gson = new Gson( );
            ArrayList<Books> libros = new ArrayList<Books>( );
            String getJson = ServiciosConstantes.COREBD + busqueda  + ServiciosConstantes.COREJSON + ServiciosConstantes.COREKEY;
            String jsonRta="";
            try
            {
                 jsonRta= ClientServicios.getHtml( getJson );
                
            }
            catch( Exception e )
            {
                e.printStackTrace( );
                jsonRta="";

            }
            if( jsonRta!="" )
            {
                libros = analizdorJsonCore(jsonRta);                
            }
            return gson.toJson( libros );
        }
    
    public static ArrayList<Books> analizdorJsonCore(String entrada)
    {
        ArrayList<Books> librosJson = new ArrayList<Books>( );
        Object obj=JSONValue.parse(entrada);
        JSONObject entryInformation = JSONObject.fromObject( obj );
        JSONArray records =  JSONArray.fromObject(entryInformation.get( "ListRecords" ));
                
          for (Object rtaRecord : records) 
          {
              JSONObject cmRecord = JSONObject.fromObject( rtaRecord ) ;
              CoreDTO libroJson = generarBookLibro(cmRecord);
              Books book = new Books( BasesConstantes.CORE );
              book.setColeccion( BasesConstantes.CORE );
              book.setCore( libroJson );
              //Books.saveBook( book );
              librosJson.add( book );
          }
          
        return librosJson;
    }
    
    public static CoreDTO generarBookLibro(JSONObject varJsonObject)
    {
        CoreDTO objetoLibro = new CoreDTO( );
        
        JSONObject objetoRecord = JSONObject.fromObject( varJsonObject.get( "record" ) );
        JSONObject objetoMetadata = JSONObject.fromObject( objetoRecord.get( "metadata" ) );
        MetadataDTO objMetadataDTO = new MetadataDTO( );
        JSONObject objetoOai_dc = JSONObject.fromObject( objetoMetadata.get( "oai_dc:dc" ) );
       
        String strCreator="";
        String strdate="";
        String strDescription="";
        String strFormat="";
        String stridentifier="";
        String strSource="";
        String strTitle="";
                
        try
        {
            strCreator = objetoOai_dc.getString( "dc:creator" );
        }
        catch( Exception e ){}
        try
        {
            strdate = objetoOai_dc.getString( "dc:date" );
        }
        catch( Exception e ){}
        try
        {
            strDescription = objetoOai_dc.getString( "dc:identifier" );
        }
        catch( Exception e ){}
        try
        {
            strFormat = objetoOai_dc.getString( "dc:format" );
        }
        catch( Exception e ){}
        try
        {
            stridentifier = objetoOai_dc.getString( "dc:identifier" );
        }
        catch( Exception e ){}
        
        try
        {
            strSource = objetoOai_dc.getString( "dc:source" );
        }
        catch( Exception e ){}
        
        try
        {
            strTitle = objetoOai_dc.getString( "dc:title" );
        }
        catch( Exception e ){}
        
        objMetadataDTO.setCreator( strCreator );
        objMetadataDTO.setDate( strdate );
        objMetadataDTO.setDescription( strDescription );
        objMetadataDTO.setFormat( strFormat );
        objMetadataDTO.setIdentifier( stridentifier );
        objMetadataDTO.setSource( strSource );
        objMetadataDTO.setTitle( strTitle );
        objetoLibro.setMetadata( objMetadataDTO );
        
        JSONObject objetoHeader = JSONObject.fromObject( objetoRecord.get( "header" ) );
        HeaderDTO objHeaderDTO = new HeaderDTO( );
        objHeaderDTO.setCore_repositoryIdentifier( objetoHeader.getString( "core:repositoryIdentifier" ) );
        objHeaderDTO.setIdentifier( objetoHeader.getString( "identifier" ) );
        
        objetoLibro.setHeader( objHeaderDTO );
        
        return objetoLibro;
    }
    
    public static JSONArray darNueroJsonArray()
    {
        return new JSONArray( );
    }
    

}
