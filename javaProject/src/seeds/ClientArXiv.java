package seeds;

import java.io.StringReader;
import java.io.StringWriter;
import java.util.Iterator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import models.BasesConstantes;
import models.Books;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

import com.google.gson.Gson;
import com.sun.org.apache.xerces.internal.parsers.DOMParser;

public class ClientArXiv
{

    public static String buscarEnBdArxiv( String autor )
    {
        String respuesta;
        try
        {
            respuesta = ClientServicios.getHtml( ServiciosConstantes.ARXIVAUTOR + autor );
        }
        catch( Exception e )
        {
            e.printStackTrace( );
            respuesta = "";
        }
        analizarRespuesta( respuesta );
        return respuesta;
    }

    @SuppressWarnings("unchecked")
    private static void analizarRespuesta( String strRta )
    {
        try
        {
            /*
             * Duplicated properties with REGEX
             */
            Gson gson = new Gson( );
            JSONArray information = new JSONArray( );
            JSONObject attributes = new JSONObject( );
            attributes.put( "authors", "<name>(.*)</name>" );
            attributes.put( "categories", "<category\\sscheme=\"[^\"]+\"\\sterm=\"([^\"]+)\"" );
            attributes.put( "category", "<arxiv:primary_category.*term=\"([^\"]+)\"" );

            /*
             * Parser for single properties
             */
            DOMParser parser = new DOMParser( );

            InputSource is = new InputSource( );
            is.setCharacterStream( new StringReader( strRta ) );

            parser.parse( is );
            Document doc = parser.getDocument( );

            // Get the document's root XML node
            NodeList root = doc.getElementsByTagName( "entry" );
            for( int x = 0; x < root.getLength( ); x++ )
            {
                JSONObject entryInformation = new JSONObject( );
                JSONObject mainObject = new JSONObject( );
                mainObject.put( "coleccion", BasesConstantes.ARXIV );
                Node exec = root.item( x );
                // String execType = getNodeAttr("type", exec);
                String nodeString = ClientServicios.nodeToString( exec );

                Iterator<String> keys = attributes.keys( );

                while( keys.hasNext( ) )
                {
                    String key = keys.next( );
                    Pattern attributePattern = Pattern.compile( ( String )attributes.get( key ), Pattern.MULTILINE );
                    Matcher codigo = attributePattern.matcher( nodeString );
                    JSONArray respuestas = new JSONArray( );
                    while( codigo.find( ) )
                    {
                        String rta = codigo.group( 1 );
                        respuestas.add( rta );
                    }
                    entryInformation.put( key, respuestas );
                }

                NodeList nodes = exec.getChildNodes( );
                String[] singleAttributes = { "summary", "updated", "title", "published" };
                for( int i = 0; i < singleAttributes.length; i++ )
                {
                    entryInformation.put( singleAttributes[ i ], ClientServicios.getNodeValue( singleAttributes[ i ].toLowerCase( ), nodes ) );
                }
                mainObject.put( "arxiv", entryInformation );
                Books newBook = gson.fromJson( mainObject.toString( ), Books.class );
                //Books.saveBook( newBook );

                information.add( entryInformation );
            }

        }
        catch( Exception e )
        {
            e.printStackTrace( );
        }
    }

}
