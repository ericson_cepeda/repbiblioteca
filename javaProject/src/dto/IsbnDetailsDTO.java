package dto;

public class IsbnDetailsDTO
{
    private String change_time;
    private String price_time;
    private String edition_info;
    private String language;
    private String physical_description_text;
    private String lcc_number;
    private String dewey_decimal_normalized;
    private String dewey_decimal;
    public IsbnDetailsDTO(  )
    {
        this.change_time = "";
        this.price_time = "";
        this.edition_info = "";
        this.language = "";
        this.physical_description_text = "";
        this.lcc_number = "";
        this.dewey_decimal_normalized = "";
        this.dewey_decimal = "";
    }
    
    public String getChange_time( )
    {
        return change_time;
    }
    public void setChange_time( String change_time )
    {
        this.change_time = change_time;
    }
    public String getPrice_time( )
    {
        return price_time;
    }
    public void setPrice_time( String price_time )
    {
        this.price_time = price_time;
    }
    public String getEdition_info( )
    {
        return edition_info;
    }
    public void setEdition_info( String edition_info )
    {
        this.edition_info = edition_info;
    }
    public String getLanguage( )
    {
        return language;
    }
    public void setLanguage( String language )
    {
        this.language = language;
    }
    public String getPhysical_description_text( )
    {
        return physical_description_text;
    }
    public void setPhysical_description_text( String physical_description_text )
    {
        this.physical_description_text = physical_description_text;
    }
    public String getLcc_number( )
    {
        return lcc_number;
    }
    public void setLcc_number( String lcc_number )
    {
        this.lcc_number = lcc_number;
    }
    public String getDewey_decimal_normalized( )
    {
        return dewey_decimal_normalized;
    }
    public void setDewey_decimal_normalized( String dewey_decimal_normalized )
    {
        this.dewey_decimal_normalized = dewey_decimal_normalized;
    }
    public String getDewey_decimal( )
    {
        return dewey_decimal;
    }
    public void setDewey_decimal( String dewey_decimal )
    {
        this.dewey_decimal = dewey_decimal;
    }
    
}
