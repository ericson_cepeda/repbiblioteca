package dto;

public class CoreDTO
{
    private HeaderDTO header;
    private MetadataDTO metadata;
    
    public CoreDTO(  )
    {
        this.header = new HeaderDTO( );
        this.metadata = new MetadataDTO( );
    }
    public HeaderDTO getHeader( )
    {
        return header;
    }
    public void setHeader( HeaderDTO header )
    {
        this.header = header;
    }
    public MetadataDTO getMetadata( )
    {
        return metadata;
    }
    public void setMetadata( MetadataDTO metadata )
    {
        this.metadata = metadata;
    }
    
    
}
