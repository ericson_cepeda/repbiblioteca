package dto;

import net.sf.json.JSONArray;

public class ArXivDTO
{
    private JSONArray authors;
    private JSONArray categories;
    private JSONArray category;
    private String summary;
    private String updated;
    private String title;
    private String published;
    
    public ArXivDTO(  )
    {
        this.authors = new JSONArray( );
        this.categories = new JSONArray( );
        this.category = new JSONArray( );
        this.summary = "";
        this.updated = "";
        this.title = "";
        this.published = "";
    }
    public JSONArray getAuthors( )
    {
        return authors;
    }
    public void setAuthors( JSONArray authors )
    {
        this.authors = authors;
    }
    public JSONArray getCategories( )
    {
        return categories;
    }
    public void setCategories( JSONArray categories )
    {
        this.categories = categories;
    }
    public JSONArray getCategory( )
    {
        return category;
    }
    public void setCategory( JSONArray category )
    {
        this.category = category;
    }
    public String getSummary( )
    {
        return summary;
    }
    public void setSummary( String summary )
    {
        this.summary = summary;
    }
    public String getUpdated( )
    {
        return updated;
    }
    public void setUpdated( String updated )
    {
        this.updated = updated;
    }
    public String getTitle( )
    {
        return title;
    }
    public void setTitle( String title )
    {
        this.title = title;
    }
    public String getPublished( )
    {
        return published;
    }
    public void setPublished( String published )
    {
        this.published = published;
    }
}
