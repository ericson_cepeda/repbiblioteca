package dto;

public class Open_Table_DTO
{
    private String title;
    private String label;
    private String pagenum;
    private String level;
    
    public Open_Table_DTO( )
    {
        this.title="";
        this.label="";
        this.pagenum="";
        this.level="";
    }

    public String getTitle( )
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public String getLabel( )
    {
        return label;
    }

    public void setLabel( String label )
    {
        this.label = label;
    }

    public String getPagenum( )
    {
        return pagenum;
    }

    public void setPagenum( String pagenum )
    {
        this.pagenum = pagenum;
    }

    public String getLevel( )
    {
        return level;
    }

    public void setLevel( String level )
    {
        this.level = level;
    }
}
