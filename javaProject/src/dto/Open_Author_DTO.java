package dto;

public class Open_Author_DTO
{
    private String url;
    private String name;
    
    public Open_Author_DTO( )
    {
        this.url="";
        this.name="";
    }

    public String getUrl( )
    {
        return url;
    }

    public void setUrl( String url )
    {
        this.url = url;
    }

    public String getName( )
    {
        return name;
    }

    public void setName( String name )
    {
        this.name = name;
    }
}
