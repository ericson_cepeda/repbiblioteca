package dto;

public class IsbnBdDTO
{
    private String bookId;
    private String isbn;
    private String isbn13;
    private String Title;
    private String TitleLong;
    private String AuthorsText;
    private String PublisherText;
    private IsbnDetailsDTO Details;
    
    
    public IsbnBdDTO( )
    {
        this.bookId = "";
        this.isbn = "";
        this.isbn13 = "";
        this.Title = "";
        this.TitleLong = "";
        this.AuthorsText = "";
        this.PublisherText = "";
        this.Details = new IsbnDetailsDTO( ); 
    }

    public String getBookId( )
    {
        return bookId;
    }

    public void setBookId( String bookId )
    {
        this.bookId = bookId;
    }

    public String getIsbn( )
    {
        return isbn;
    }

    public void setIsbn( String isbn )
    {
        this.isbn = isbn;
    }

    public String getIsbn13( )
    {
        return isbn13;
    }

    public void setIsbn13( String isbn13 )
    {
        this.isbn13 = isbn13;
    }

    public String getTitle( )
    {
        return Title;
    }

    public void setTitle( String title )
    {
        Title = title;
    }

    public String getTitleLong( )
    {
        return TitleLong;
    }

    public void setTitleLong( String titleLong )
    {
        TitleLong = titleLong;
    }

    public String getAuthorsText( )
    {
        return AuthorsText;
    }

    public void setAuthorsText( String authorsText )
    {
        AuthorsText = authorsText;
    }

    public String getPublisherText( )
    {
        return PublisherText;
    }

    public void setPublisherText( String publisherText )
    {
        PublisherText = publisherText;
    }

    public IsbnDetailsDTO getDetails( )
    {
        return Details;
    }

    public void setDetails( IsbnDetailsDTO details )
    {
        Details = details;
    }
    
    
}
