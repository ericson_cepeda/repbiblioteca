package dto;

public class GoogleDTO
{
    private String kind;
    //private String accessInfo;
    private String selfLink;
    private GoogleVolumenDTO volumeInfo;
    private String etag;
    private String searchInfo;
    private String id;

    
    public GoogleDTO( )
    {
        this.kind = "";
        //this.accessInfo = "";
        this.selfLink = "";
        this.volumeInfo = new GoogleVolumenDTO();
        this.etag = "";
        this.searchInfo = "";
        this.id = "";

    }
    
    public GoogleVolumenDTO getVolumengoogleDTO( )
    {
        return volumeInfo;
    }

    public void setVolumengoogleDTO( GoogleVolumenDTO volumengoogleDTO )
    {
        this.volumeInfo = volumengoogleDTO;
    }

    public String getKind( )
    {
        return kind;
    }

    public void setKind( String kind )
    {
        this.kind = kind;
    }

//    public String getAccessInfo( )
//    {
//        return accessInfo;
//    }
//
//    public void setAccessInfo( String accessInfo )
//    {
//        this.accessInfo = accessInfo;
//    }

    public String getSelfLink( )
    {
        return selfLink;
    }

    public void setSelfLink( String selfLink )
    {
        this.selfLink = selfLink;
    }

//    public String getVolumeInfo( )
//    {
//        return volumeInfo;
//    }
//
//    public void setVolumeInfo( String volumeInfo )
//    {
//        this.volumeInfo = volumeInfo;
//    }

    public String getEtag( )
    {
        return etag;
    }

    public void setEtag( String etag )
    {
        this.etag = etag;
    }

    public String getSearchInfo( )
    {
        return searchInfo;
    }

    public void setSearchInfo( String searchInfo )
    {
        this.searchInfo = searchInfo;
    }

    public String getId( )
    {
        return id;
    }

    public void setId( String id )
    {
        this.id = id;
    }

}
