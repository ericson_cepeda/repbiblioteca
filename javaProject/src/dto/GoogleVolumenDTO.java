package dto;

import java.util.ArrayList;

import models.GoogleIndustryId;

public class GoogleVolumenDTO
{
    private String pageCount;
    private String infoLink;
    private String printType;
    private ArrayList<String> authors;
    private String publisher;
    private String canonicalVolumeLink;
    private String title;
    private String subtitle;
    private String previewLink;
    private ArrayList<String>  categories;
    private String contentVersion;
    private String publishedDate;
    private String description;
    private String averageRating;
    private String ratingsCount;
    private ArrayList<GoogleIndustryId> industryIdentifiers;
    private String language;

    public GoogleVolumenDTO( )
    {
        this.pageCount = "";
        this.infoLink = "";
        this.printType = "";
        this.authors = new ArrayList<String>( );
        this.publisher = "";
        this.canonicalVolumeLink = "";
        this.title = "";
        this.subtitle = "";
        this.previewLink = "";
        this.categories = new ArrayList<String>( );
        this.contentVersion = "";
        this.publishedDate = "";
        this.language = "";
        this.industryIdentifiers = new ArrayList<GoogleIndustryId>( );
        this.averageRating = "";
        this.ratingsCount = "";
        this.description = "";
    }

    public String getSubtitle( )
    {
        return subtitle;
    }

    public void setSubtitle( String subtitle )
    {
        this.subtitle = subtitle;
    }

    public String getRatingsCount( )
    {
        return ratingsCount;
    }

    public void setRatingsCount( String ratingsCount )
    {
        this.ratingsCount = ratingsCount;
    }

    public String getDescription( )
    {
        return description;
    }

    public void setDescription( String description )
    {
        this.description = description;
    }

    public String getPublisher( )
    {
        return publisher;
    }

    public void setPublisher( String publisher )
    {
        this.publisher = publisher;
    }

    public String getAverageRating( )
    {
        return averageRating;
    }

    public void setAverageRating( String averageRating )
    {
        this.averageRating = averageRating;
    }

    public String getPageCount( )
    {
        return pageCount;
    }

    public void setPageCount( String pageCount )
    {
        this.pageCount = pageCount;
    }

    public String getInfoLink( )
    {
        return infoLink;
    }

    public void setInfoLink( String infoLink )
    {
        this.infoLink = infoLink;
    }

    public String getPrintType( )
    {
        return printType;
    }

    public void setPrintType( String printType )
    {
        this.printType = printType;
    }

    public ArrayList<String> getAuthors( )
    {
        return authors;
    }

    public void setAuthors( ArrayList<String> authors )
    {
        this.authors = authors;
    }

    public String getCanonicalVolumeLink( )
    {
        return canonicalVolumeLink;
    }

    public void setCanonicalVolumeLink( String canonicalVolumeLink )
    {
        this.canonicalVolumeLink = canonicalVolumeLink;
    }

    public String getTitle( )
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public String getPreviewLink( )
    {
        return previewLink;
    }

    public void setPreviewLink( String previewLink )
    {
        this.previewLink = previewLink;
    }

//    public String getImageLinks( )
//    {
//        return imageLinks;
//    }
//
//    public void setImageLinks( String imageLinks )
//    {
//        this.imageLinks = imageLinks;
//    }

    public ArrayList<String>  getCategories( )
    {
        return categories;
    }

    public void setCategories( ArrayList<String> categories )
    {
        this.categories = categories;
    }

    public String getContentVersion( )
    {
        return contentVersion;
    }

    public void setContentVersion( String contentVersion )
    {
        this.contentVersion = contentVersion;
    }

    public String getPublishedDate( )
    {
        return publishedDate;
    }

    public void setPublishedDate( String publishedDate )
    {
        this.publishedDate = publishedDate;
    }

    public ArrayList<GoogleIndustryId> getIndustryIdentifiers( )
    {
        return industryIdentifiers;
    }

    public void setIndustryIdentifiers( ArrayList<GoogleIndustryId> industryIdentifiers )
    {
        this.industryIdentifiers = industryIdentifiers;
    }

    public String getLanguage( )
    {
        return language;
    }

    public void setLanguage( String language )
    {
        this.language = language;
    }
}
