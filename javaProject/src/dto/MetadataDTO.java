package dto;

public class MetadataDTO
{
    private String creator;
    private String format;
    private String source;
    private String date;
    private String identifier;
    private String description;
    private String title;
    
    public MetadataDTO(  )
    {
        this.creator = "";
        this.format = "";
        this.source = "";
        this.date = "";
        this.identifier = "";
        this.description = "";
        this.title = "";
    }
    public String getCreator( )
    {
        return creator;
    }
    public void setCreator( String creator )
    {
        this.creator = creator;
    }
    public String getFormat( )
    {
        return format;
    }
    public void setFormat( String format )
    {
        this.format = format;
    }
    public String getSource( )
    {
        return source;
    }
    public void setSource( String source )
    {
        this.source = source;
    }
    public String getDate( )
    {
        return date;
    }
    public void setDate( String date )
    {
        this.date = date;
    }
    public String getIdentifier( )
    {
        return identifier;
    }
    public void setIdentifier( String identifier )
    {
        this.identifier = identifier;
    }
    public String getDescription( )
    {
        return description;
    }
    public void setDescription( String description )
    {
        this.description = description;
    }
    public String getTitle( )
    {
        return title;
    }
    public void setTitle( String title )
    {
        this.title = title;
    }  
}
