package dto;

import java.util.ArrayList;

public class OpenDTO
{
    private String ISBN;
    private ArrayList<Open_Author_DTO> author;
    private String dewey_decimal_class;
    private String lc_classifications;
    private String title;
    private ArrayList<Open_link_DTO> links;
    private ArrayList<Open_Table_DTO> table_of_contents;
    private ArrayList<String> google;
    private ArrayList<String> lccn;
    private ArrayList<String> openlibrary;
    private ArrayList<String> isbn_13;
    private ArrayList<String> amazon;
    private ArrayList<String> isbn_10;
    private ArrayList<String> oclc;
    private ArrayList<String> librarything;
    private ArrayList<String> goodreads;
    
    public OpenDTO(  )
    {
        this.ISBN = "";
        this.author = new ArrayList<Open_Author_DTO>();
        this.dewey_decimal_class = "";
        this.lc_classifications = "";
        this.title = "";
        this.links = new ArrayList<Open_link_DTO>( );
        this.table_of_contents = new ArrayList<Open_Table_DTO>( );
        this.google = new ArrayList<String>();
        this.lccn = new ArrayList<String>();
        this.openlibrary = new ArrayList<String>();
        this.isbn_13 = new ArrayList<String>();
        this.amazon = new ArrayList<String>();
        this.isbn_10 = new ArrayList<String>();
        this.oclc = new ArrayList<String>();
        this.librarything = new ArrayList<String>();
        this.goodreads = new ArrayList<String>();
    }

    public String getISBN( )
    {
        return ISBN;
    }

    public void setISBN( String iSBN )
    {
        ISBN = iSBN;
    }

    public ArrayList<Open_Author_DTO> getAuthor( )
    {
        return author;
    }

    public void setAuthor( ArrayList<Open_Author_DTO> urlAuthor )
    {
        this.author = urlAuthor;
    }

    public String getDewey_decimal_class( )
    {
        return dewey_decimal_class;
    }

    public void setDewey_decimal_class( String dewey_decimal_class )
    {
        this.dewey_decimal_class = dewey_decimal_class;
    }

    public String getLc_classifications( )
    {
        return lc_classifications;
    }

    public void setLc_classifications( String lc_classifications )
    {
        this.lc_classifications = lc_classifications;
    }

    public String getTitle( )
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }

    public ArrayList<Open_link_DTO> getLinks( )
    {
        return links;
    }

    public void setLinks( ArrayList<Open_link_DTO> links )
    {
        this.links = links;
    }

    public ArrayList<Open_Table_DTO> getTable_of_contents( )
    {
        return table_of_contents;
    }

    public void setTable_of_contents( ArrayList<Open_Table_DTO> table_of_contents )
    {
        this.table_of_contents = table_of_contents;
    }

    public ArrayList<String> getGoogle( )
    {
        return google;
    }

    public void setGoogle( ArrayList<String> google )
    {
        this.google = google;
    }

    public ArrayList<String> getLccn( )
    {
        return lccn;
    }

    public void setLccn( ArrayList<String> lccn )
    {
        this.lccn = lccn;
    }

    public ArrayList<String> getOpenlibrary( )
    {
        return openlibrary;
    }

    public void setOpenlibrary( ArrayList<String> openlibrary )
    {
        this.openlibrary = openlibrary;
    }




    public ArrayList<String> getIsbn_13( )
    {
        return isbn_13;
    }

    public void setIsbn_13( ArrayList<String> isbn_13 )
    {
        this.isbn_13 = isbn_13;
    }




    public ArrayList<String> getAmazon( )
    {
        return amazon;
    }

    public void setAmazon( ArrayList<String> amazon )
    {
        this.amazon = amazon;
    }

    public ArrayList<String> getIsbn_10( )
    {
        return isbn_10;
    }




    public void setIsbn_10( ArrayList<String> isbn_10 )
    {
        this.isbn_10 = isbn_10;
    }

    public ArrayList<String> getOclc( )
    {
        return oclc;
    }

    public void setOclc( ArrayList<String> oclc )
    {
        this.oclc = oclc;
    }

    public ArrayList<String> getLibrarything( )
    {
        return librarything;
    }

    public void setLibrarything( ArrayList<String> librarything )
    {
        this.librarything = librarything;
    }

    public ArrayList<String> getGoodreads( )
    {
        return goodreads;
    }

    public void setGoodreads( ArrayList<String> goodreads )
    {
        this.goodreads = goodreads;
    }

    
}
