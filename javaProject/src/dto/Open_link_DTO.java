package dto;

public class Open_link_DTO
{
    private String url;
    private String title;
    
    public Open_link_DTO( )
    {
        this.url="";
        this.title="";
    }

    public String getUrl( )
    {
        return url;
    }

    public void setUrl( String url )
    {
        this.url = url;
    }

    public String getTitle( )
    {
        return title;
    }

    public void setTitle( String title )
    {
        this.title = title;
    }
}
