package dto;

public class HeaderDTO
{
    private String core_repositoryIdentifier;
    private String identifier;
    
    public HeaderDTO( )
    {
        this.core_repositoryIdentifier = "";
        this.identifier = "";
    }
    
    public String getCore_repositoryIdentifier( )
    {
        return core_repositoryIdentifier;
    }
    public void setCore_repositoryIdentifier( String core_repositoryIdentifier )
    {
        this.core_repositoryIdentifier = core_repositoryIdentifier;
    }
    public String getIdentifier( )
    {
        return identifier;
    }
    public void setIdentifier( String identifier )
    {
        this.identifier = identifier;
    }
    
}
