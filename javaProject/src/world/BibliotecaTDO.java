package world;

import java.util.ArrayList;

public class BibliotecaTDO
{

    /**
     * Clasificaci�on Dewey:
     */
    private String clasificacionDewey;
    /**
     * Autor Personal:
     */
    private ArrayList<String> autorPersonal;
    /**
     * Titulo:
     */
    private String titulo;
    /**
     * Datos Publicacion:
     */
    private String datosPublicacion;
    /**
     * Desc. Fisica:
     */
    private String descFisica;
    /**
     * Contenido Parcial:
     */
    private String contenidoParcial;
    /**
     * Contenido:
     */
    private String contenido;
    /**
     * Restriccion Acceso:
     */
    private String restriccionAcceso;
    /**
     * Ref. donacion:
     */
    private ArrayList<String> refDonacion;
    /**
     * Nota de Bibliografia:
     */
    private String notaDeBibliografia;
    /**
     * Curso audiencia:
     */
    private ArrayList<String> cursoAudiencia;
    /**
     * Nota de Reproduccion:
     */
    private String notaDeReproduccion;
    /**
     * Nota General:
     */
    private String notaGeneral;
    /**
     * En Biblioteca(s):
     */
    private String enBiblioteca;
    /**
     * Bulletin heading:
     */
    private String bulletinHeading;
    /**
     * Materia Personal:
     */
    private ArrayList<String> materiaPersonal;
    /**
     * Materia General:
     */
    private ArrayList<String> materiaGeneral;
    /**
     * Materia Geografica:
     */
    private ArrayList<String> materiaGeografica;
    /**
     * Autor Secundario:
     */
    private ArrayList<String> autorSecundario;

    public BibliotecaTDO( )
    {
        this.clasificacionDewey = "";
        this.autorPersonal = new ArrayList<String>( );
        this.titulo = "";
        this.datosPublicacion = "";
        this.descFisica = "";
        this.contenidoParcial = "";
        this.contenido = "";
        this.restriccionAcceso = "";
        this.refDonacion = new ArrayList<String>( );
        this.notaDeBibliografia = "";
        this.cursoAudiencia = new ArrayList<String>( );
        this.notaDeReproduccion = "";
        this.notaGeneral = "";
        this.enBiblioteca = "";
        this.bulletinHeading = "";
        this.materiaPersonal = new ArrayList<String>( );
        this.materiaGeneral = new ArrayList<String>( );
        this.materiaGeografica = new ArrayList<String>( );
        this.autorSecundario = new ArrayList<String>( );

    }

    public String getClasificacionDewey( )
    {
        return clasificacionDewey;
    }

    public void setClasificacionDewey( String clasificacionDewey )
    {
        this.clasificacionDewey = clasificacionDewey;
    }

    public ArrayList<String>  getAutorPersonal( )
    {
        return autorPersonal;
    }

    public void setAutorPersonal( ArrayList<String> autorPersonal )
    {
        this.autorPersonal = autorPersonal;
    }

    public String getTitulo( )
    {
        return titulo;
    }

    public void setTitulo( String titulo )
    {
        this.titulo = titulo;
    }

    public String getDatosPublicacion( )
    {
        return datosPublicacion;
    }

    public void setDatosPublicacion( String datosPublicacion )
    {
        this.datosPublicacion = datosPublicacion;
    }

    public String getDescFisica( )
    {
        return descFisica;
    }

    public void setDescFisica( String descFisica )
    {
        this.descFisica = descFisica;
    }

    public String getContenidoParcial( )
    {
        return contenidoParcial;
    }

    public void setContenidoParcial( String contenidoParcial )
    {
        this.contenidoParcial = contenidoParcial;
    }

    public String getContenido( )
    {
        return contenido;
    }

    public void setContenido( String contenido )
    {
        this.contenido = contenido;
    }

    public String getRestriccionAcceso( )
    {
        return restriccionAcceso;
    }

    public void setRestriccionAcceso( String restriccionAcceso )
    {
        this.restriccionAcceso = restriccionAcceso;
    }

    public ArrayList<String> getRefDonacion( )
    {
        return refDonacion;
    }

    public void setRefDonacion( ArrayList<String> refDonacion )
    {
        this.refDonacion = refDonacion;
    }

    public String getNotaDeBibliografia( )
    {
        return notaDeBibliografia;
    }

    public void setNotaDeBibliografia( String notaDeBibliografia )
    {
        this.notaDeBibliografia = notaDeBibliografia;
    }

    public ArrayList<String> getCursoAudiencia( )
    {
        return cursoAudiencia;
    }

    public void setCursoAudiencia( ArrayList<String> cursoAudiencia )
    {
        this.cursoAudiencia = cursoAudiencia;
    }

    public String getNotaDeReproduccion( )
    {
        return notaDeReproduccion;
    }

    public void setNotaDeReproduccion( String notaDeReproduccion )
    {
        this.notaDeReproduccion = notaDeReproduccion;
    }

    public String getNotaGeneral( )
    {
        return notaGeneral;
    }

    public void setNotaGeneral( String notaGeneral )
    {
        this.notaGeneral = notaGeneral;
    }

    public String getEnBiblioteca( )
    {
        return enBiblioteca;
    }

    public void setEnBiblioteca( String enBiblioteca )
    {
        this.enBiblioteca = enBiblioteca;
    }

    public String getBulletinHeading( )
    {
        return bulletinHeading;
    }

    public void setBulletinHeading( String bulletinHeading )
    {
        this.bulletinHeading = bulletinHeading;
    }

    public ArrayList<String> getMateriaPersonal( )
    {
        return materiaPersonal;
    }

    public void setMateriaPersonal( ArrayList<String> materiaPersonal )
    {
        this.materiaPersonal = materiaPersonal;
    }

    public ArrayList<String> getMateriaGeneral( )
    {
        return materiaGeneral;
    }

    public void setMateriaGeneral( ArrayList<String> materiaGeneral )
    {
        this.materiaGeneral = materiaGeneral;
    }

    public ArrayList<String> getMateriaGeografica( )
    {
        return materiaGeografica;
    }

    public void setMateriaGeografica( ArrayList<String> materiaGeografica )
    {
        this.materiaGeografica = materiaGeografica;
    }

    public ArrayList<String> getAutorSecundario( )
    {
        return autorSecundario;
    }

    public void setAutorSecundario( ArrayList<String> autorSecundario )
    {
        this.autorSecundario = autorSecundario;
    }

}
