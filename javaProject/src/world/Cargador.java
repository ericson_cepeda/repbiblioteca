package world;

import java.util.ArrayList;

import models.Books;
import models.GoogleIndustryId;
import seeds.ClientArXiv;
import seeds.ClientCORE;
import seeds.ClientGoogleBooks;
import seeds.ClientISBNDB;
import seeds.ClientOPEN;
import seeds.ClientServicios;

import com.google.gson.Gson;

public class Cargador
{
    private static String servicioUrl = "http://157.253.238.114:8080/serializationbiblio/views/index/";
    private static String servicioMetodo = "getNextBook.jsp?callback=jsonCallback&_=1351286739326";
    private Gson gson;

    public Cargador( ) throws Exception
    {
        gson = new Gson( );
    }

    private String llamarCore( String busqueda )
    {
        busqueda = busqueda.replace( " ", "%20" );
        return ClientCORE.buscarEnBdCore( busqueda );
    }

    public String llamarArxiv( String busqueda )
    {
        busqueda = busqueda.replace( " ", "%20" );
        return ClientArXiv.buscarEnBdArxiv( busqueda );
    }

    private ArrayList<Books> llamarGoogleBooks( String busqueda )
    {
        busqueda = busqueda.replace( " ", "%20" );
        return ClientGoogleBooks.buscarEnBdGoogle( busqueda );
    }

    public String llamarOpen( String busqueda )
    {
        busqueda = busqueda.replace( " ", "%20" );
        return ClientOPEN.buscarEnBdOpen( busqueda ); 
    }
    
    private String llamarISBNFULL( String busqueda )
    {
        busqueda = busqueda.replace( " ", "%20" );
        return ClientISBNDB.buscarEnBdISBN_Full( busqueda );
    }
    
    private String llamarISBN( String busqueda )
    {
        busqueda = busqueda.replace( " ", "%20" );
        return ClientISBNDB.buscarEnBdISBN_ISBN( busqueda );
    }
    
    public int llamarnext( )
    {
        String servicio = servicioUrl + servicioMetodo;
        String entrada = "";
        try
        {
            entrada = ClientServicios.getHtml( servicio );
            if( entrada.equals( "-1" ) )
            {
                return -1;
            }
        }
        catch( Exception e )
        {
            e.printStackTrace( );
        }

        BibliotecaTDO objetobiblio = gson.fromJson( entrada, BibliotecaTDO.class );
        buscarEnFuentesTitulo( objetobiblio.getTitulo( ) );
        return 1;
    }

    public void buscarEnFuentes( BibliotecaTDO objBiblioteca )
    {
        ArrayList<String> strAutorPersona = objBiblioteca.getAutorPersonal( );

        ArrayList<Books> googleAutor = new ArrayList<Books>( );

        for( String stringRta : strAutorPersona )
        {
            llamarArxiv( stringRta );
            llamarCore( stringRta );
            llamarISBNFULL(stringRta );
            googleAutor = llamarGoogleBooks( stringRta );
            for( Books books : googleAutor )
            {
                ArrayList<GoogleIndustryId> ids = books.getGoogleBooks( ).getVolumengoogleDTO( ).getIndustryIdentifiers( );
                for( GoogleIndustryId googleIndustryId : ids )
                {
                    llamarOpen( googleIndustryId.getIdentifier( ) );
                    llamarISBN( googleIndustryId.getIdentifier( ) );
                }
            }

        }

        String strTitulo = objBiblioteca.getTitulo( );
        ArrayList<Books> googleTitulo = new ArrayList<Books>( );

        llamarArxiv( strTitulo );
        llamarCore( strTitulo );
        llamarISBNFULL(strTitulo );
        googleTitulo = llamarGoogleBooks( strTitulo );
        
        for( Books books : googleTitulo )
        {
            ArrayList<GoogleIndustryId> ids = books.getGoogleBooks( ).getVolumengoogleDTO( ).getIndustryIdentifiers( );
            for( GoogleIndustryId googleIndustryId : ids )
            {
                llamarOpen( googleIndustryId.getIdentifier( ) );
                llamarISBN( googleIndustryId.getIdentifier( ) );
            }
        }
        

    }

    
    public void buscarEnFuentesTitulo( String titulo )
    {

        String strTitulo = titulo;
        ArrayList<Books> googleTitulo = new ArrayList<Books>( );

        llamarArxiv( strTitulo );
        llamarCore( strTitulo );
        llamarISBNFULL(strTitulo );
        googleTitulo = llamarGoogleBooks( strTitulo );
        
        for( Books books : googleTitulo )
        {
            ArrayList<GoogleIndustryId> ids = books.getGoogleBooks( ).getVolumengoogleDTO( ).getIndustryIdentifiers( );
            for( GoogleIndustryId googleIndustryId : ids )
            {
                llamarOpen( googleIndustryId.getIdentifier( ) );
                llamarISBN( googleIndustryId.getIdentifier( ) );
            }
        }
        

    }
}
