package world;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import seeds.ClientArXiv;
import seeds.ClientServicios;


public class DeweyClassification
{
    
    public static List<String> categoriasDewey(String dewey)
    {
            //System.out.println(ClientServicios.getHtml( "http://dewey.info/class/641.01/about.en.rdf" ));
        List<String> cats = new ArrayList<String>( );
            try
            {
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse("http://dewey.info/class/"+dewey+"/about.en.rdf");
                
                doc.getDocumentElement().normalize();
                
                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                NodeList nListPs = doc.getElementsByTagName("skos:broader");
                NodeList nListCats = doc.getElementsByTagName("skos:prefLabel");
                System.out.println("-----------------------");
                while(nListPs.item( 0 )!=null)
                {
                    System.out.println(doc.getDocumentURI( ));
                    Node nNode = nListCats.item(0);
                    if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                        String nodevalue = ClientServicios.getNodeValue( nNode );
                             System.out.println(nodevalue);
                             cats.add( nodevalue );
                     }
                    Node nodoB = nListPs.item( 0 ) ;
                    Element eElement = (Element) nodoB;
                    String docBroader = eElement.getAttribute( "rdf:resource" );
                    Pattern attributePattern = Pattern.compile( "(http://dewey.info/class/[0-9\\.]+)", Pattern.MULTILINE );
                    Matcher codigo = attributePattern.matcher( docBroader );
                    if(codigo.find( ))
                    {
                        docBroader = codigo.group(1);
                    }
                    System.out.println("cambio a: "+docBroader );
                    doc = dBuilder.parse(docBroader+"/about.en.rdf");
                    nListPs = doc.getElementsByTagName("skos:broader");
                    nListCats = doc.getElementsByTagName("skos:prefLabel");
                    System.out.println("-----------------------");

                }                
            }
            catch( Exception e )
            {
                // TODO: handle exception
            }

            
            return cats;
            
    }
    
    

    /** FOR TESTING ONLY
     * @param args
     */
    public static void main( String[] args )
    {
        // TODO Auto-generated method stub
        try
        {
            //System.out.println(ClientServicios.getHtml( "http://dewey.info/class/641.01/about.en.rdf" ));
            
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse("http://dewey.info/class/333.76/about.en.rdf");
            
            doc.getDocumentElement().normalize();
            List<String> cats = new ArrayList<String>( );
            System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
            NodeList nListPs = doc.getElementsByTagName("skos:broader");
            NodeList nListCats = doc.getElementsByTagName("skos:prefLabel");
            System.out.println("-----------------------");
            while(nListPs.item( 0 )!=null)
            {
                System.out.println(doc.getDocumentURI( ));
                Node nNode = nListCats.item(0);
                if (nNode.getNodeType() == Node.ELEMENT_NODE) {
                    String nodevalue = ClientServicios.getNodeValue( nNode );
                         System.out.println(nodevalue);
                         cats.add( nodevalue );
                 }
                Node nodoB = nListPs.item( 0 ) ;
                Element eElement = (Element) nodoB;
                String docBroader = eElement.getAttribute( "rdf:resource" );
                Pattern attributePattern = Pattern.compile( "(http://dewey.info/class/[0-9\\.]+)", Pattern.MULTILINE );
                Matcher codigo = attributePattern.matcher( docBroader );
                if(codigo.find( ))
                {
                    docBroader = codigo.group(1);
                }
                System.out.println("cambio a: "+docBroader );
                doc = dBuilder.parse(docBroader+"/about.en.rdf");
                nListPs = doc.getElementsByTagName("skos:broader");
                nListCats = doc.getElementsByTagName("skos:prefLabel");
                System.out.println("-----------------------");

            }
            

            System.out.println("Arreglo a retornar: ");
            for(String cat:cats)
            {
                System.out.println(cat);
            }
            
            
        }
        catch( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

    }

}
