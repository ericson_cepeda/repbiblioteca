package scheduler;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

import world.Cargador;

public class ControlCargador
{
    private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool( 1 );
    private static ControlCargador INSTANCE;
    private Cargador obCargador;
    private int cadaCuanto;
    private int desdeCuando;
    private int porCuantosSegundos;
    
    public ControlCargador( int prmCadaCuanto, int prmDesdeCuando, int prmPorCuantosSegundos ) throws Exception
    {
        cadaCuanto = prmCadaCuanto;
        desdeCuando = prmDesdeCuando;
        porCuantosSegundos = prmPorCuantosSegundos;
        obCargador = new Cargador( );
        
    }
    
    public static ControlCargador getInstance(){
        if (INSTANCE == null)
            try
            {
                INSTANCE = new ControlCargador(2,1,120);
            }
            catch( Exception e )
            {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        return INSTANCE;
    }
    
    public void CorrerUnaHora( )
    {
        final Runnable tarea = new Runnable( )
        {
            public void run( )
            {
                int rta = 0;
                
                rta = obCargador.llamarnext( );
                if( rta == -1 )
                {
                    scheduler.shutdown( );
                }
            }
        };
        
        final ScheduledFuture<?> beeperHandle = scheduler.scheduleAtFixedRate( tarea, desdeCuando, cadaCuanto, TimeUnit.SECONDS );
        scheduler.schedule( new Runnable( )
        {
            public void run( )
            {
                beeperHandle.cancel( true );
                scheduler.shutdown( );
            }
        },  porCuantosSegundos, TimeUnit.SECONDS );
    }
    
    public boolean estaCorriendo()
    {
        return scheduler.isShutdown( );
    }
    

}
