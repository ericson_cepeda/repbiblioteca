package controllers;

import java.util.ArrayList;

import models.Books;
import scheduler.ControlCargador;

import com.google.gson.JsonObject;
import comparacion.Comparador;
import comparacion.DtoRespuesta;

public class IndexController
{
    public static  ArrayList<DtoRespuesta> getBook(String title){
        
        Comparador.getInstance( ).resultadoBusqueda( title );
        
        return Comparador.getInstance( ).getRtaLibreria( );
    }
   
    public static String getStatistics(){
        
        JsonObject response = Books.getStatistics( );
        
        ControlCargador control;
        try
        {
            control = ControlCargador.getInstance( );
            if(!control.estaCorriendo( ))
                control.CorrerUnaHora( );
        }
        catch( Exception e )
        {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        //response.put( "booksQuantity", ser.getObjetoBiblioteca( ).size( ) );
        //response.put( "statsDB", connector.getMongoDB( ).getStats( ).toMap( ) );
        
        return response.toString( );
    }
}
